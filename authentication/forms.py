from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from .models import *

ATS = {"class": "form-control", "required": "required"}

class AccountForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.TextInput(attrs=ATS),
        label=_("ელ.ფოსტა"))
    name = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("სახელი და გვარი"))
    phone = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("ტელეფონის ნომერი (xxx-xxx-xxx)"), validators=(
        RegexValidator(r"[\d]{3}-[\d]{3}-[\d]{3}", message=_(
        "არასწორი ნომრის ფორმატი")),))
    password = forms.CharField(widget=forms.PasswordInput(attrs=ATS),
        label=_("პაროლი"))

    class Meta:
        model = Account
        fields = ("name", "phone", "email", "password",)
        exclude = ("is_active", "is_admin", "is_staff", "last_login", "basket",
            "perms", "address",)

class AccountUpdateForm(AccountForm):
    email = None
    password = None

    age = forms.IntegerField(widget=forms.NumberInput(attrs=ATS),
        label=_("ასაკი"))
    sex = forms.ChoiceField(widget=forms.Select(attrs=ATS),
        label=_("სქესი"), choices=ASC)

    class Meta:
        model = Account
        fields = ("name", "phone", "age", "sex")
