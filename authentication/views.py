from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.views import generic
from django.contrib.auth import authenticate
from django.contrib.auth import logout
from django.contrib.auth import login
from django.contrib import messages

from django.utils.translation import ugettext_lazy as _

from shop.models import Product
from .models import *
from .forms import *

NOT_ACTIVE_MESSAGE = _("თქვენი ანგარიში არ არის აქტიური, მიყევით ბმულს ელ.ფოსტაზე")
BAD_CREDENTIALS = _("თქვენს მიერ შეყვანილი მონაცემები არასწორია")
NO_ACCOUNT = _("მომხმარებელი არ არსებობს, გაიარეთ რეგისტრაცია :)")
SIGNUP_SUCCESS = _("თქვენ წარმატებით დარეგისტრირდით, გააქტიურეთ ანგარიში ბმულით ელ.ფოსტაზე")


# Create your views here.
class SignUpView(generic.CreateView):
    template_name = "authentication/signin.html"
    model = Account
    form_class = AccountForm
    object = None

    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax():
            return self.ajax(request, *args, **kwargs)
        return super().dispatch(request, *args, **kwargs)

    def ajax(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            entry = form.save()
            entry.set_password(entry.password)
            entry.save()
            entry.send_activation_mail(request.META["HTTP_HOST"])
            messages.add_message(self.request, messages.SUCCESS, SIGNUP_SUCCESS)
            return JsonResponse({"redirect": reverse("landing")})
        else:
            return JsonResponse(form.errors.as_json(), safe=False)


class SigninView(generic.TemplateView):
    template_name = 'authentication/signin.html'

    def hook_next(self):
        if "QUERY_STRING" in self.request.META:
            qs = self.request.META['QUERY_STRING']
            if "next" in self.request.META['QUERY_STRING']:
                return self.request.META["QUERY_STRING"]
        return None

    def red_next(self):
        qs = self.hook_next()
        if qs is not None:
            return redirect(qs.replace("next=", ""))
        return redirect(reverse("auth:profile"))

    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax():
            return self.ajax(request, *args, **kwargs)
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, email=None):
        return Account.objects.filter(email=email).first()

    def ajax(self, request, *args, **kwargs):
        data = {}
        account = self.get_object(request.POST.get("email"))
        if not account:
            data['error'] = NO_ACCOUNT
        elif not account.is_active:
            data['error'] = NOT_ACTIVE_MESSAGE
        elif not account.check_password(request.POST.get("password")):
            data['error'] = BAD_CREDENTIALS
        else:
            login(request, account)
            data['redirect'] = self.red_next().url
        return JsonResponse(data)

    def post(self, request, *args, **kwargs):
        email = request.POST.get("email", "nomail@email.com")
        password = request.POST.get("password", "pswd")
        try:
            entry = Account.objects.get(email=email)
            if not entry.is_active:
                messages.add_message(request, messages.WARNING, NOT_ACTIVE_MESSAGE)
                return render(request, self.template_name,
                    self.get_context_data(**kwargs))
            entry = authenticate(username=email, password=password)
            if entry is not None:
                login(request, entry)
                return self.red_next()
            messages.add_message(request, messages.WARNING, BAD_CREDENTIALS)
        except Account.DoesNotExist:
            messages.add_message(request, messages.WARNING, NO_ACCOUNT)
        return render(request, self.template_name, self.get_context_data(**kwargs))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['next'] = self.hook_next()
        context['form'] = AccountForm()
        return context


class SocAuthView(generic.TemplateView):
    
    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax():
            return self.ajax(request, *args, **kwargs)
        return redirect("landing")

    def ajax(self, request, *args, **kwargs):
        name = request.GET.get("name", None)
        email = request.GET.get("email", None)
        fbid = request.GET.get("id", None)
        data = {"redirect": reverse("landing")}
        # start processing if all components are present
        if name and email and fbid:
            entry = None
            try:
                # check if same user exists
                entry = Account.objects.get(email=email)
                if entry.fbid != fbid:
                    entry.fbid = fbid
                entry.save()
            except Account.DoesNotExist:
                # if not below block is executed
                entry = Account.objects.create(email=email,
                    name=name, fbid=fbid, phone="", is_active=True)
                # password is not set @ all
            login(request, entry)
            data['redirect'] = reverse("auth:profile")
        return JsonResponse(data)


class ProfileView(generic.DetailView):
    template_name = "authentication/profile.html"
    object = None

    def get_object(self):
        return Account.objects.get(pk=self.request.user.pk)

    def post(self, request, *args, **kwargs):
        form = AccountUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            request.user.save()
            messages.add_message(request, messages.SUCCESS,
                str(_("თქვენი ინფორმაცია წარმატებით განახლდა")))
        else:
            messages.add_message(request, messages.WARNING,
                str(_("ინფორმაციის განახლება ვერ მოხერხდა")))
        return redirect("auth:profile")


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['updateForm'] = AccountUpdateForm(instance=self.object)
        print(self.request.user.basket.is_empty())
        return context

class SignoutView(generic.RedirectView):
    def get(self, request, *args, **kwargs):
        logout(request)
        messages.add_message(request, messages.WARNING,
            _("თქვენ გახვედით ვებ გვერდიდან, გელით უკან :)"))
        return redirect(reverse("landing"))

class ActivationView(generic.RedirectView):
    def get(self, request, *args, **kwargs):
        object = get_object_or_404(Account, token=self.kwargs.get('token', None))
        object.is_active = True
        object.save()
        messages.add_message(request, messages.SUCCESS,
            str(_("თქვენი ანგარიში წარმატებით გააქტიურდა")))
        if authenticate(username=object.email, password=object.password):
            login(object, request)
            return redirect(reverse("auth:profile"))
        return redirect(reverse("auth:profile"))

class PasswordUpdateView(generic.TemplateView):
    template_name = "authentication/password.html"

    def post(self, request, *args, **kwargs):
        if request.POST.get("step_email", None):
            entry = None
            try:
                entry = Account.objects.get(email=request.POST.get("email", None))
            except Account.DoesNotExist:
                messages.add_message(request, messages.SUCCESS, 
                    str(_("სამწუხაროდ, მომხმარებელი ამ ელ.ფოსტით არ არის დარეგისტრირებული")))
                return redirect("auth:password")
            if entry:
                entry.send_reset_link(request.META["HTTP_HOST"])
                messages.add_message(request, messages.SUCCESS, str(_(
                    "ელ.ფოსტაზე გამოგიგზავნეთ პაროლის შესაცვლელი ბმული")))
                return redirect("landing")
        elif request.POST.get("step_password", None):
            token = kwargs.get("token", None)
            password = request.POST.get("password", None)
            entry = None
            print(request.POST, kwargs)
            try:
                entry = Account.objects.get(token=token)
            except Account.DoesNotExist:
                messages.add_message(request, messages.SUCCESS, str(_(
                    "პაროლის შესაცვლელი ბმული არასწორია")))
                return redirect("auth:password")
            if entry and password:
                entry.set_password(password)
                entry.save()
                messages.add_message(request, messages.SUCCESS, str(_(
                    "პაროლი წარმატებით შეიცვალა, გაიარეთ ავტორიზაცია")))
                return redirect(reverse("landing") + "#signin")
            elif not password:
                messages.add_message(request, messages.SUCCESS, str(_(
                    "გთხოვთ მიუთითოთ პაროლი")))
                return redirect("auth:password", kwargs={"token": entry.token})

    def get(self, request, *args, **kwargs):
        token = kwargs.get("token", None)
        print(Account.objects.filter(token=token))
        if token and not Account.objects.filter(token=token).count():
            messages.add_message(request, messages.SUCCESS, str(_(
                    "პაროლის შესაცვლელი ბმული არასწორია")))
        return render(request, self.template_name, self.get_context_data(
            step = 1 if kwargs.get("token", None) else 0))


class NewsLetterView(generic.RedirectView):
    def post(self, request, *args, **kwargs):
        email = request.POST.get("email", None)
        status = CollectedEmail.objects.get_or_create(email=email)[1]
        if status:
            messages.add_message(request, messages.SUCCESS, 
                str(_("თქვენი ელ ფოსტა დავაფიქსირე :)")))
        else:
            messages.add_message(request, messages.SUCCESS,
                str(_("თქვენი ელ.ფოსტა უკვე ჩემს ბაზაშია")))
        return redirect(request.META['HTTP_REFERER'])


class MessageView(generic.TemplateView):
    template_name="authentication/messages.html"

    def get_messages(self, **kwargs):
        return self.request.user.owned_messages.filter(**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['messages_seen'] = self.get_messages(seen=True)
        context['messages_new'] = self.get_messages(seen=False)
        return context