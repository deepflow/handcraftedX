from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    name = 'authentication'
    verbose_name = _("ავტორიზაცია")
