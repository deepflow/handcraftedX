from django.conf.urls import url
from django.contrib.auth.decorators import login_required as x

from .views import *

urlpatterns = [
    url(r"signup/$", SignUpView.as_view(), name="signup"),
    url(r"signin/$", SigninView.as_view(), name="signin"),
    url(r"socauth/$", SocAuthView.as_view(), name="socauth"),
    url(r"signout/$", x(SignoutView.as_view()), name="signout"),
    url(r"profile/$", x(ProfileView.as_view()), name='profile'),

    url(r"activeX/(?P<token>[\w\W]+)/$", ActivationView.as_view(), name="activate"),
    url(r"password/((?P<token>[\w\W]+)/)?$", PasswordUpdateView.as_view(), name="password"),
    url(r"newsletter/$", NewsLetterView.as_view(), name="newsletter"),

    # url(r"messageto/(?P<token>[\w\d]+)/((?P<prt>[\d])/(?P<ppk>[\d]+)/)?$", x(MessageTo.as_view()), name="message_to"),

    # MESSAGING
    url(r"messages/((?P<thread>[\d]+)/)?$", x(MessageView.as_view()), name="messages"),
]
