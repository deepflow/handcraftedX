from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import Account, CollectedEmail, Review, InternalMessage

x = admin.site.register

class InternalMessageAdmin(admin.ModelAdmin):
    '''administrator for Internal Message'''
    list_display = ("owner", "seen", "created")

class AccountAdmin(admin.ModelAdmin):
    '''administrator for Account model'''
    fieldsets = (
        (None, {
            "classes": ("extrapretty",),
            "fields": ("password", "email", "name", "phone", "basket",
                       "address", "is_active", "is_staff", "is_admin",),
        }),
        (_("უფლებები"), {
            "fields": ("groups", "user_permissions",)
        }),
        (_("META"), {
            "fields": ("token", )
        })
    )
    filter_horizontal = ('user_permissions', 'groups')
    list_display = ("email", "name", "is_active", "is_staff")
    search_fields = ("email", "namep")

class CollectedMailAdmin(admin.ModelAdmin):
    '''administrator for CollectedMail model'''
    list_display = ("email", "created")

x(Account, AccountAdmin)
x(Review)
x(CollectedEmail, CollectedMailAdmin)
x(InternalMessage, InternalMessageAdmin)