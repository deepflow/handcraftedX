from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from common.models import SnapShot
from common.helpers import generate_token
from selling.models import Seller
from basket.models import Basket

ASC = (
    ("-", _("-----")),
    ("0", _("მამრობითი")),
    ("1", _("მდედრობითი")),
)

class EmailAutomation:
    
    def send_email(self, subject, message, receivers):
        EmailAutomation.send_email(subject, message, receivers)
    
    def send_activation_mail(self, host):
        subject = str(_("ანაგარიშის გააქტიურება"))
        message = str(_("ანგარიშის გასააქტიურებლად მიყევით ბმულს\n{0}".format(
            "http://{0}{1}".format(host, reverse("auth:activate", kwargs={
            "token":self.token})))))
        self.send_email(subject, message, self.email)

    def send_reset_link(self, host):
        subject = str(_("პაროლის აღდგენა"))
        message = str(_("პაროლის აღსადგენათ მიჰყევით ბმულს\n{0}".format(
            "http://{0}{1}{2}".format(host, reverse("auth:password"),
                self.token))))
        self.send_email(subject, message, self.email)

    def notify_successful_order(self):
        subject = str(_("წარმატებული შეკვეთა - handx.org"))
        html_content = render_to_string('checkout/successful_order.html')
        text_content = strip_tags(html_content)
        msg = EmailMultiAlternatives(subject, "deepflowllc@gmail.com", text_content, [self.email, ])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

    @staticmethod
    def send_email(subject, message, receivers):
        """
            subject - subject of the email
            message - message body of the email
            receivers - receiver list or receiver of the email
            send_email(str, str, list) -> None
        """
        try:
            if isinstance(receivers, str):
                receivers = [receivers, ]
            email = EmailMessage(subject, message, to=receivers)
            email.send()
        except Exception as ge:
            print("---Exception during sending email[core]", ge)
            return None

    @staticmethod
    def notify_order(order, state="გაურკვეველი", host="handx.org"):
        emails = Account.get_order_admin_emails()
        subject = _("ახალი შეკვეთა - {0}".format(state))
        message = "http://{0}{1}".format(host, reverse(
            "admin:checkout_order_change", args=[order.pk, ]))
        EmailAutomation.send_email(subject, message, emails)


class AccountManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError(_("მომხმარებელს უნდა ჰქონდეს ელ.ფოსტა"))
        account = self.model(email=self.normalize_email(email))
        account.set_password(password)
        account.save()
        return account

    def create_superuser(self, email, password, **kwargs):
        account = self.create_user(email, password, **kwargs)
        account.is_admin = True
        account.is_staff = True
        account.is_superuser = True
        account.is_active = True
        account.save()
        return account


class Review(SnapShot):
    content = models.TextField(_("ტექსტი"))
    author = models.ForeignKey("Account", verbose_name=_("ავტორი"))

    def __str__(self):
        return self.content[:64]

    class Meta:
        verbose_name = _("კომენტარი")
        verbose_name_plural = _("კომენტარები")
        ordering = ["-created"]

class Account(AbstractBaseUser, PermissionsMixin, SnapShot, EmailAutomation):
    #CUSTOM USER FIELDS
    email = models.EmailField(_("ელ. ფოსტა"), unique=True)
    name = models.CharField(_("სახელი და გვარი"), max_length=128)
    phone = models.CharField(_("ტელეფონის ნომერი"), max_length=19)
    token = models.CharField(_("მომხმარებლის token"), max_length=10, default=generate_token)

    basket = models.ForeignKey("basket.Basket", verbose_name=_("კალათა"), blank=True, null=True, on_delete=models.SET_NULL)
    address = models.ForeignKey("checkout.Address", verbose_name=_("მისამართი"), blank=True, null=True, on_delete=models.SET_NULL)

    sex = models.CharField(_("სქესი"), default="-", choices=ASC, max_length=1)
    age = models.IntegerField(_("ასაკი"), blank=True, default=0)
    fbid = models.CharField(_("fb იდენთიფიკატორი"), max_length=128, blank=True, null=True)

    objects = AccountManager()
    USERNAME_FIELD = "email" #automatically adds to REQUIRED_FIELDS
    REQUIRED_FIELDS = []

    @property
    def unseen_message_count(self):
        return self.owned_messages.filter(seen=False).count()

#    @property
#    def basket(self):
#        if not self._basket:
#            self._basket = Basket.objects.create()
#            self.save()
#            return self._basket
#        return self._basket

    def get_fname(self):
        return self.name.split(' ')[0]

    def get_address(self):
        address = ", ".join([self.address.line1, self.address.line2])
        if self.address.line3 != "":
            address += ", " + self.address.line3
        address += " ({0})".format(self.address.phone)
        return address

    def get_seller(self):
        seller = Seller.objects.filter(account=self)
        return seller[0] if seller.count() > 0 else None

    def get_orders(self):
        return self.order_set.all().filter(status__in=['0', '1', '2'])

    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)


    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email
    #END OF DJANGO REQUIRED FIELDS AND METHODS

    def get_absolute_url(self):
        return reverse("auth:profile")

    def get_seller_item_count(self):
        return self.product_set.count()

    def __str__(self):
        if self.name != "":
            return self.name
        return self.email

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.basket = Basket.objects.create()
        super().save(*args, **kwargs)
    
    @staticmethod
    def get_order_admin_emails():
        receivers = Account.objects.filter(user_permissions__codename="email_order_creation")
        return receivers.values_list("email", flat=True)

    class Meta:
        verbose_name = _("ანგარიში")
        verbose_name_plural = _("ანგარიშები")
        permissions = (
            ("email_order_creation", _("შეუძლია მიიღოს შეტყობინება შეკვეთის შესახებ")),
        )

class CollectedEmail(SnapShot):
    email = models.EmailField()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _("ელ.ფოსტა")
        verbose_name_plural = _("ელ.ფოსტები")

class InternalMessage(SnapShot):
    owner = models.ForeignKey(Account, blank=True, related_name="owned_messages")
    sender = models.ForeignKey(Account, blank=True, null=True, related_name="sent_messages")
    content = models.TextField(_("კონტენტი"))
    seen = models.BooleanField(default=False)
    token = models.ForeignKey("common.Tokenizer", blank=True, null=True, on_delete=models.SET_NULL)

    def get_default_action(self):
        if self.token:
            val = self.token.get_val("action")
            return val if val else None
        return None

    @staticmethod
    def mark_message_as_read(mpk):
        try:
            entry = InternalMessage.objects.get(pk=mpk)
            entry.seen = True
            entry.save()
        except InternalMessage.DoesNotExist:
            return _("მესიჯი ვერ მოიძებნა :(")

    @staticmethod
    def delete_message(mpk):
        try:
            InternalMessage.objects.get(pk=mpk).delete()
            return _("მესიჯი წარმატებით წავშალე :)")
        except InternalMessage.DoesNotExist:
            return _("მესიჯი ვერ მოიძებნა :(")

    def __str__(self):
        return str(self.owner)

    class Meta:
        verbose_name = _("შიდა შეტყობინება")
        verbose_name_plural = _("შიდა შეტყობინებები")
        ordering = ('-created', )

