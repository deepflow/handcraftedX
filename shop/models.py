from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal as D
from django.shortcuts import get_object_or_404
from django.utils.html import strip_tags, format_html
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q, F
from django.utils.text import slugify
from django.utils import timezone

# for aggregation purpuses
from django.db.models import Max, Min

from tinymce import models as tmceModels
from common.models import SnapShot
from common.helpers import generate_random_number
from selling.models import Seller

from common.helpers import latinify
from common.models import Tokenizer

from math import ceil
import datetime as dt
import requests

class Image(SnapShot):
    image = models.ImageField(verbose_name=_("სურათი"), upload_to='shopImages/')
    product = models.ForeignKey("Product", null=True)

    def __str__(self):
        if self.product is None:
            return _("გამოუყენებელი სურათი")
        return str(self.product)

    class Meta:
        verbose_name = _("სურათი")
        verbose_name_plural = _("სურათები")
        ordering = ['-created']

class ProductOption(SnapShot):
    name = models.CharField(_("ვარიანტის სახელი"), max_length=128)
    price = models.DecimalField(_("ვარიანტის ფასი"), decimal_places=2, max_digits=5)
    tax = models.DecimalField(_("დანამატი"), decimal_places=2, max_digits=5, blank=True)
    quantity = models.IntegerField(_("ვარიანტის რაოდენობა"))

    is_special = models.BooleanField(_("სპეციალური შეთავაზება"), default=False)
    sp_price = models.DecimalField(_("ვარიანტის ფასი"), decimal_places=2, max_digits=5, blank=True, default=D(0))
    exp_date = models.DateField(default=dt.datetime.now)

    product = models.ForeignKey("Product", blank=True, null=True)

    def is_available(self, q):
        '''
            checks if given quantity is available for this option
            is_available(int) -> bool
        '''
        return True if q <= self.quantity else False

    def get_price(self):
        if self.is_special:
            return self.sp_price + self.tax
        return self.price + self.tax

    def get_special_price(self):
        # returns min price for special option
        return D(ceil(self.get_price() + D(3)))

    def get_special_copy(self, price, exp_date):
        copy = self
        copy.is_special = True
        copy.sp_price = price if price > self.get_special_price() else self.get_special_price()
        copy.exp_date = exp_date
        copy.pk = None
        copy.quantity = 1
        copy.save()
        return copy.pk

    def get_sname(self):
        return ' '.join(self.name.split(' ')[:2])

    def get_seller(self):
        return self.product.get_seller()

    def get_absolute_url(self):
        return self.product.get_absolute_url()

    def __str__(self):
        return "{0}-{1}".format(str(self.product), self.name)

    def save(self, *args, **kwargs):
        # tax is calculated like - 3.5% of price plus delivery service price
        # self.tax = self.price * D(0.035) + D(3.00)
        self.tax = self.price * D(0.035)
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("ვარიანტი")
        verbose_name_plural = _("ვარიანტები")
        ordering = ['-created']

class Product(SnapShot):
    visible = models.BooleanField(_("გამოჩნდეს ვებ-გვერდზე"), default=False, help_text=_(
        "თუ მონიშნულია, გამოჩნდება ვებგვერდზე, თუ არა, შენახულია დრაფტად"))
    publish_at = models.DateTimeField(_("გამოქვეყნების დრო"), default=timezone.now, help_text=_(
        "თუ არ გსურთ პროდუქტის გამოქვეყნების დაგეგმვა, დატოვეთ უცვლელი, ან აირჩიეთ სასურველი დრო"))
    share = models.BooleanField(_("გაზიარება facebook-ზე"), default=False, help_text=_(
        "თუ მონიშნულია, გამოქვეყნების დროს გაზიარდება facebook-ზე"))
    share_at = models.DateTimeField(_("გაზიარების დრო"), default=timezone.now, help_text=_(
        "თუ არ გსურთ გაზიარების დაგეგმვა, დატოვეთ უცვლელი ან აირჩიეთ სასურველი დრო"))

    name = models.CharField(_("ზოგადი დასახელება"), max_length=64)
    desc = tmceModels.HTMLField(verbose_name=_("აღწერა"))
    rank = models.IntegerField(_("რანგი/ხმები"), default=0)
    views = models.IntegerField(_('ნახვების რაოდენობა'), default=0)

    #relationships
    reviews = models.ManyToManyField("authentication.Review", blank=True)
    owner = models.ForeignKey("authentication.Account", blank=True, default=1)
    keywords = models.ManyToManyField("common.Keyword", blank=True, verbose_name=_("საკვანძო სიტყვები"))

    sku = models.CharField(_("მარაგის შენახვის ერთეული"), blank=True, default="", max_length=8)
    slug = models.SlugField(allow_unicode=True, max_length=64, db_index=True, default="", blank=True)
    likerAccounts = models.ManyToManyField("authentication.Account", blank=True, related_name="liker")

    def get_seller(self):
        try:
            return Seller.objects.get(account=self.owner)
        except Seller.DoesNotExist:
            return _("ავტორი ვერ მოიძებნა")

    def get_seller_related(self, count):
        '''get active products for this owner except self'''
        qs = Product.get_actives().filter(owner=self.owner).exclude(
            pk=self.pk).order_by("?")
        return qs[:count]

    def get_related(self, count):
        '''get active & related products'''
        ques = ~Q(owner=self.owner)
        qs = Product.get_actives().filter(ques)
        return qs[:count]

    def get_sname(self):
        return ' '.join(self.name.split(' ')[0:2])

    def first_image(self):
        if self.image_set.all().count() > 0:
            return self.image_set.all()[0].image

    def get_sdesc(self):
        '''return short description - first 10 words of whole desc'''
        return " ".join(strip_tags(self.desc).split(" ")[:10])

    def get_images(self):
        return self.image_set.all()

    def get_absolute_url(self):
        return reverse("shop:product", kwargs={'slug':self.slug})

    def get_sto(self):
        return self.productoption_set.filter(is_special=False)

    # PROPERTIES
    @property
    def like(self):
        return self.rank
    
    @like.setter
    def like(self, value):
        self.rank += value
        self.save()

    @property
    def view(self):
        return self.views
    
    @view.setter
    def view(self, value):
        self.views += value
        self.save()

    @property
    def is_new(self):
        # if product is newer than 1 week mark it as new - dummy logic
        return True if (timezone.now() - self.created).days <= 7 else False
    
    @property
    def is_hot(self):
        # if product views is more than 75% then it's hot
        vmax = Product.objects.aggregate(Max("views"))["views__max"]
        vmin = Product.objects.aggregate(Min("views"))["views__min"]
        vmax -= vmin # normalize
        vmin = 0 # normalize
        if self.views >= vmax * 0.75:
            return True
        return False
    #END OF PROPERTIES

    # PRICE RELATED METHODS
    def price_range(self, formated=True):
        prices = []
        for option in self.productoption_set.all():
            prices.append(option.get_price())
        #THIS CODE ADDED DUE TO EXCEPTION - ON NOT ADDED PRICE
        if not len(prices):
            if not formated:
                return [0, 0]
            else:
                return "GEL"
        #END OF ISSUED CODE
        if not formated:
            return [min(prices), max(prices)]
        if len(prices) == 1:
            return "GEL {0}".format(min(prices))
        else:
            return "GEL {0} - {1}".format(min(prices), max(prices))

    def price_average(self):
        average = sum(self.price_range(formated=False))
        average /= 2
        return "GEL {0}".format(average)
    price_average.short_description = _("საშუალო ფასი")
    # END OF PRICE RELATED METHODS

    # 3rd party methods
    def share_on_facebook(self):
        try:
            fbat = Tokenizer.objects.get(token="fb_access_token")
            share_data = {"access_token": fbat.get_val("access_token"),
                "link": "http://{0}{1}".format("handx.org", self.get_absolute_url()),}
            requests.post(fbat.get_val("link"), share_data)
        except Exception:
            pass
    # END OF THE 3RD PARTY METHODS

    @staticmethod
    def review(ppk, r, u):
        """
            given product PK, review and user
            method adds reviewer to db
            review(int, Review, str) -> str
        """
        try:
            object = Product.objects.get(pk=ppk)

            return _("მადლობა, თქვენი აზრი მნიშვნელოვანია ჩვენთვის")
            return _("თქვენ უკვე მოიწონეთ ეს პროდუქტი :)")
        except Product.DoesNotExist:
            return _("სამწუხაროდ ვერ ვაფიქსირებ თქვენს ხმას, სცადეთ მოგვიანებით :|")

    @staticmethod
    def upvote(ppk, u):
        """
            based on product pk and user pk function stores liker to db
            upvote(int, int/Account) -> str
        """
        try:
            from authentication.models import Account
            object = Product.objects.get(pk=ppk)
            if (isinstance(u, str) or isinstance(u, int)) and u != "None": #in case u comes from ajax call
                u = Account.objects.get(pk=u) #get account entry manually
            elif u == "None" or u is None: #user musst sign in
                return _("ხმის გთხოვთ გაიაროთ ავტორიზაცია")
            if u not in object.likerAccounts.all():
                object.likerAccounts.add(u)
                object.like = 1 #already saves this assignment
                return _("თქვენი ხმა მიღებულია, მადლობა გამოხატვისთვის")
            return _("თქვენ უკვე მოიწონეთ ეს პროდუქტი :)")
        except Product.DoesNotExist:
            return _("სამწუხაროდ ვერ ვაფიქსირებ თქვენს ხმას, გაიარეთ ავტორიზაცია :|")

    def __str__(self):
        return self.name

    # def save(self, *args, **kwargs):
    #     if self.pk is None:
    #         self.slug = slugify(latinify(self.name) + "-" + str(self.pk), allow_unicode=True)
    #     super().save(*args, **kwargs)

    # QUERYSET METHODS
    @staticmethod
    def get_actives(qs=None):
        query = {"visible": True, "publish_at__lte": timezone.now()}
        return qs.filter(**query) if qs else Product.objects.filter(**query)
    # END OF QUERYSET METHODS

    # django admin methods
    def get_shop_url(self):
        return format_html("<a target='_blank' href='{0}'>{1}</a>".format(self.get_absolute_url(),
            _("იხილეთ ვებ გვერდზე")))
    get_shop_url.short_description = _("ბმულები")
    # end of django admin methods

    class Meta:
        verbose_name = _("პროდუქტი")
        verbose_name_plural = _("პროდუქტები")
        ordering = ['-created',]


@receiver(post_save, sender=Product)
def post_product_save(sender, instance, created, **kwargs):
    if created or instance.sku == "":
        length = 8 - len(str(instance.pk))
        instance.sku = generate_random_number(length) + str(instance.pk)
        instance.slug = slugify(latinify(instance.name) + "-" + str(instance.pk), allow_unicode=True)
        instance.save()
