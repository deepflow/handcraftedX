from rest_framework import serializers
from sorl.thumbnail import get_thumbnail

from .models import Product

class ProductSerializer(serializers.ModelSerializer):
    name = serializers.CharField()
    desc = serializers.CharField()
    images = serializers.SerializerMethodField()
    price_range = serializers.SerializerMethodField()
    options = serializers.SerializerMethodField()
    keywords = serializers.SerializerMethodField()

    def get_keywords(self, obj):
        return [{"name":str(k), "url":k.get_absolute_url()} for k in \
            obj.keywords.all()]

    def get_options(self, obj):
        return [{"pk": o.pk, "name": o.name, "price": o.get_price(), "quant": o.quantity} \
            for o in obj.productoption_set.filter(is_special=False)]

    def get_price_range(self, obj):
        return obj.price_range()

    def get_images(self, obj):
        return [get_thumbnail(i.image, "385x385", crop="center", quality=70).url \
             for i in obj.image_set.all()]

    class Meta:
        model = Product
        fields = ("name", "desc", "images", "price_range", "options",
            "keywords", "pk")
