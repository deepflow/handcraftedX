from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from ajax_select.admin import AjaxSelectAdmin
from .models import *
from .forms import ProductAdminForm

x = admin.site.register

# INLINES
class ImagesInline(admin.TabularInline):
    model = Image
    extra = 1
    verbose_name = _("სურათი")
    verbose_name_plural = _("სურათები")


class ProductOptionsInline(admin.StackedInline):
    model = ProductOption
    extra = 1
    verbose_name = _("ვარიანტი")
    verbose_name_plural = _("ვარიანტები")
    fields = ("name", "price", "quantity", )
    min_num = 1 # makes sure that @ least one option is filled 


# PRODUCT ADMIN
class ProductAdmin(AjaxSelectAdmin):
    inlines = [ImagesInline, ProductOptionsInline]
    form = ProductAdminForm

    list_display = ("name", "views", "get_shop_url", "price_average", "visible")
    list_filter = ("visible", "owner")

    def get_fieldsets(self, request, obj):
        if request.user.is_admin:
            return (
                (_("სამართავი იფნორმაცია"), {
                    "fields": (("visible", "publish_at"), ("share", "share_at"),)
                }),
                (_("ზოგადი ინფორმაცია"), {
                    "fields": ("owner", "name", "keywords", "raw", "desc", "slug"),
                }),
            )
        return (
            (_("სამართავი იფნორმაცია"), {
                "fields": (("visible", "publish_at"), ("share", "share_at"),)
            }),
            (_("ზოგადი ინფორმაცია"), {
                "classes": ("extrapretty",),
                "fields": ("name", "desc", "keywords", "raw"),
            }),
        )

    def get_list_display(self, request):
        if request.user.is_admin:
            return ("name", "views", "get_shop_url", "price_average", "visible", "get_seller")
        return ("name", "views", "get_shop_url", "price_average", "visible")

    def get_queryset(self, request):
        if request.user.is_admin:
            return Product.objects.all()
        return Product.objects.filter(owner=request.user)

    def save_model(self, request, obj, form, change):
        if not request.user.is_admin: #in case admin reassigns product
            obj.owner = request.user
        super(ProductAdmin, self).save_model(request, obj, form, change)

    def save_related(self, request, form, formsets, change):
        # method overrided to save keywords, as they are related objects
        super().save_related(request, form, formsets, change)
        form.instance.keywords.add(*form.get_raw_keywords())

#IMAGE ADMIN
class ImageAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        '''
            filters and gets all owner images, not others',
            if owner is admin - shows all images
        '''
        if request.user.is_admin:
            return Image.objects.all()
        images = Image.objects.none()
        for prod in Product.objects.filter(owner=request.user):
            images |= Image.objects.filter(product=prod)
        return images

# PRODUCT OPTION ADMINS
class ProductOptionFilter(admin.SimpleListFilter):
    title = _("სპეციალური პროდუქტი")
    parameter_name = "is_special"

    def lookups(self, request, model_admin):
        return (
            (True, _("სპეციალური ვარიანტი")),
            (False, _("სტანდარტული ვარიანტი"))
        )

    def queryset(self, request, queryset):
        if self.value():
            return ProductOption.objects.filter(is_special=self.value())
        return queryset


class ProductOptionAdmin(admin.ModelAdmin):
    fieldsets = (
        (_("მთავარი"), {
            "fields": ('name', 'price', 'quantity', 'product',)
        }),
        (_("მეტა"), {
            "fields": ("is_special", )
        })
    )
    list_display = ("name", "is_special", "quantity", )
    readonly_fields = ("is_special", )
    list_filter = (ProductOptionFilter, )

    def get_queryset(self, request):
        if request.user.is_admin:
            return ProductOption.objects.all()
        options = ProductOption.objects.none()
        for item in Product.objects.filter(owner=request.user):
            options |= ProductOption.objects.filter(product=item)
        return options


x(Product, ProductAdmin)
x(Image, ImageAdmin)
x(ProductOption, ProductOptionAdmin)
