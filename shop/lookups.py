from ajax_select import register, LookupChannel
from common.models import Keyword

@register("keywords")
class KeywordLookup(LookupChannel):
    model = Keyword

    def get_query(self, q, request):
        return self.model.objects.filter(name__icontains=q).order_by("name")[:20]

    def format_item_display(self, item):
        return '<span class="keyword">{0}</span>'.format(item)