from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class ShopConfig(AppConfig):
    name = 'shop'
    verbose_name = _("მაღაზია")
    