from django import forms
from django.utils.translation import ugettext_lazy as _
from ajax_select.fields import AutoCompleteSelectMultipleField
from django.utils import timezone

from tinymce.widgets import TinyMCE
from .models import Product
from common.models import Keyword

ATS = {"class":"form-control", "required":"required"}

class ProductForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("პროდუქტის დასახელება"))
    desc = forms.CharField(widget=TinyMCE(attrs=ATS),
        label=_("პროდუქტის აღწერა"))
    price = forms.DecimalField(widget=forms.NumberInput(attrs=ATS),
        label=_("პროდუქტის ფასი (ლარი)"))
    quantity = forms.IntegerField(widget=forms.NumberInput(attrs=ATS),
        label=_("პროდუქტის რაოდენობა"))

    class Meta:
        model = Product
        exclude = ("images", "tax", "rank",)

class ProductAdminForm(forms.ModelForm):
    keywords = AutoCompleteSelectMultipleField("keywords", label=_("საკვანძო სიტყვები"), required=False, help_text=_("შეიყვანეთ საძიებო სიტყვა, ან დაამატეთ"))
    raw = forms.CharField(widget=forms.HiddenInput(), required=False)

    def get_raw_keywords(self):
        keywords = Keyword.objects.none()
        if len(self.cleaned_data['raw']) == 0:
            return Keyword.objects.none();
        for pk in self.cleaned_data['raw'].split("|"):
            keywords |= Keyword.objects.filter(pk=pk)
        return keywords

    def clean_keywords(self):
        keyword_pks = self.cleaned_data['keywords']
        for pk in keyword_pks:
            target = Keyword.objects.get(pk=pk)
            if Keyword.objects.filter(name=target.name, fname=target.fname).count() > 1:
                raise forms.ValidationError(_("საკვანძო სიტყვებში აღმოჩენილია 2 ან მეტი მსგავსი სიტყვა"),
                    code="invalid")
        return self.cleaned_data['keywords']

    class Meta:
        model = Product
        fields = ("__all__")

    
    class Media:
        js = ("js/admin/product_management.js", )
