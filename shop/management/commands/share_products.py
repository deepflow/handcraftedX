from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.db.models import F
from django.core.management.base import BaseCommand, CommandError
from shop.models import Product

class Command(BaseCommand):
    help = _("ყველა იმ პროდუქტის გამოქვეყნება, რომელიც დაგეგმილია მოცემული დროისთვის")

    def handle(self, *args, **options):
        try:
            # three main states available for product share - standard, planned, instant
        
            # standard -> share_at < created_at and share = False -> don't share

            # planned -> share_at > created_at and share = True and share_at < current_time -> share with cron
            planned = Product.objects.filter(share_at__gte=F("created"))\
                .filter(share=True).filter(share_at__lte=timezone.now())\
                .filter(visible=True)
            
            # share each product
            for product in planned:
                product.share_on_facebook()
            # update product sharing params
            planned.update(share=False) # modify share=False, to avoid multiple share

            #instant -> share_at < created_at and share = True -> share with cron (not truly instant)
            instant = Product.objects.filter(share_at__lte=F("created"))\
                .filter(share=True).filter(visible=True)
            # share each product
            for product in instant:
                product.share_on_facebook()
            # update product sharing params
            instant.update(share=False)
        except Exception as ge:
            print(ge)
