from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.db.models import F
from django.core.management.base import BaseCommand, CommandError
from shop.models import Product

# management command depricated - moved to get_actives@Product model
class Command(BaseCommand):
    help = _("ყველა იმ პროდუქტის გამოქვეყნება, რომელიც დაგეგმილია მოცემული დროისთვის")

    def handle(self, *args, **options):
        try:
            # if visible is checked and creation time is less than publish_at time
            # planned = Product.objects.filter(visible=True)\
            #     .filter(created__lt=F("publish_at")).filter( # this part of the query is more
            #         publish_at__lte=timezone.now())
            pass
        except Exception as ge:
            pass
