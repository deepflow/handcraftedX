import ast
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils.encoding import uri_to_iri
from django.core import serializers
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

from .models import *
from authentication.models import Review

from .forms import ProductForm
from common.helpers import get_querystring_value
from common.models import Tokenizer, Keyword
from selling.models import Seller
from authentication.models import InternalMessage, Account

class MainView(generic.ListView):
    template_name = "shop/product_list.html"
    model = Product
    queryset = Product.get_actives()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['objects'] = Product.get_actives()
        return context

class ProductListView(generic.ListView):
    template_name = "shop/product_list.html"
    queryset = Product.get_actives()
    paginate_by = 12

    def get_queryset(self):
        if "token" in self.kwargs:
            try:
                token = Tokenizer.objects.get(token=self.kwargs['token'])
                return serializers.deserialize("json", token.meta)
            except Tokenizer.DoesNotExist:
                raise Http404(_("ბმული ვერ მოიძებნა"))
        return Product.get_actives(Product.objects.order_by("-created", "views"))

class SellerView(generic.ListView):
    template_name = 'shop/seller_product_list.html'
    queryset = Product.get_actives()
    paginate_by = 20
    object = None

    def get_seller(self):
        self.object = get_object_or_404(Seller, slug=self.kwargs.get("slug", None))
        return self.object.account

    def get_queryset(self):
        self.queryset = Product.get_actives().filter(owner=self.get_seller())
        return self.queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_head'] = self.object.shopName
        context['object'] = self.object
        return context


class KeyWordProductListView(generic.ListView):
    template_name = 'shop/product_list.html'
    queryset = Product.get_actives()
    paginate_by = 12

    def get_keyword(self):
        return get_object_or_404(Keyword, pk=self.kwargs.get("pk", ""))

    def get_queryset(self):
        kwd = self.get_keyword()
        return self.queryset.filter(keywords__in=[kwd])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class ProductView(generic.DetailView):
    model = Product
    object = None

    def post(self, request, *args, **kwargs):
        Product.objects.get(pk=kwargs.get("pk", 0)).reviews.add(
            Review.objects.create(content=request.POST.get("comment", ''),
            author=request.user))
        return redirect(request.META["HTTP_REFERER"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.object.view = 1
        context['seller_related'] = self.object.get_seller_related(4)
        context['related'] = self.object.get_related(4)
        return context


class AcceptSpecialView(generic.TemplateView):
    template_name = "shop/accept_special.html"

    def extract_token(self):
        self.token = Tokenizer.get_token(self.kwargs.get("token", None))
        if not self.token:
            return None
        data = ast.literal_eval(self.token.meta)
        self.option = get_object_or_404(ProductOption, pk=data["opk"])
        self.seller = self.option.get_seller()
        return True

    def get_content(self):
        msg = _(
            '''
                გამარჯობა, თქვენს მიერ მოთხოვნილი პროდუქტი ({0}),
                მზად იქნება {1}-ში. მოთხოვნის წარმატებით დასასრულებლად
                გთხოვთ მიჰყვეთ "შემოთავაზებულ ქმედებას" და შეიძინოთ 
                თქვენი პროფილიდან მოთხოვნილი პროდუქტი
            '''.format(self.option.get_absolute_url(), self.exp_date)
        )
        return msg

    def get_msg_owner(self):
        return get_object_or_404(Account, pk=self.token.get_val("u"))

    def get(self, request, *args, **kwargs):
        self.extract_token()
        # in case seller denies the special offer
        if request.GET.get("deny", None) is not None or self.option is None:
            messages.add_message(request, messages.SUCCESS, _(
                "თქვენი პასუხი მიღებულია :)"))
            return redirect(self.option.get_absolute_url())
        # in case anwer is yes - create special offer, load form for further submission
        return render(request, self.template_name, self.get_context_data(**kwargs))

    def post(self, request, *args, **kwargs):
        self.extract_token()
        self.day_count = int(self.request.POST.get("day_count", 0))
        if self.day_count <= 0 or not self.option:
            messages.add_message(request, messages.SUCCESS, _(
                "ფორმა შეიცავს შეცდომებს, გთხოვთ სცადოთ თავიდან :)"))
            return render(request, self.template_name, self.get_context_data(**kwargs))
        # form is valid so create special option and send it to user
        self.exp_date = dt.datetime.now().date() + dt.timedelta(days=self.day_count)
        sopk = self.option.get_special_copy(D(self.token.get_val("sprice")),
            self.exp_date) # special option pk
        self.token.update(sopk=str(sopk))
        # clear relationship between token and bound message
        im = self.token.internalmessage_set.all()[0]
        im.token = None
        im.save()
        # end of the logic - bad check logic
        InternalMessage.objects.create(owner=self.get_msg_owner(),
            sender=self.request.user, content=self.get_content(),
            token=self.token)
        self.token.update(action=reverse("basket:add", kwargs={"opk": sopk, "cnt": 1}))
        self.token.update(exp_date=str(self.exp_date))
        messages.add_message(request, messages.SUCCESS, _(
            'თქვენი პასუხი წარმატებით გაიგზავნა'))
        return redirect("auth:messages")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sprice'] = self.token.get_val("sprice")
        context['name'] = str(self.option)
        context['action'] = self.option.get_absolute_url()
        return context
            
class SearchView(generic.ListView):
    template_name = "shop/search_results.html"

    def get(self, request, *args, **kwargs):
        self.get_query() # initialize query member of the view
        self.object_list = self.get_queryset()
        return render(request, self.template_name, self.get_context_data(
            query=self.query, ordering=self.ordering))

    def get_query(self):
        '''get search query given as GET arg'''
        self.query = self.request.GET.get("query", None)
        # check for empty query
        if self.query is None:
            raise Http404(_("შედეგები ცარიელი საძიებო სიტყვით ვერ მოიძებნა"))
        # check if query is multi word structure
        if len(self.query.split(" ")) > 1:
            self.query = self.query.split(" ")
        return self.query

    def get_ordering(self):
        '''method to extract ordering argument in get method'''
        ordering = self.request.GET.get("ordering", None)
        default_ordering = "-pk"
        fields_avail = ["-pk", "-views"]
        if ordering is None or ordering not in fields_avail:
            self.ordering = default_ordering
            return default_ordering
        self.ordering = ordering
        return ordering

    def get_queryset(self):
        '''
            based on query variable it filters out products based on their desc and title
            includeing their option names
        '''
        ques = Q()
        # if query is just a string
        if isinstance(self.query, str):
            ques |= Q(name__icontains=self.query)
            ques |= Q(desc__icontains=self.query)
        # else if the product is iterable list
        elif isinstance(self.query, list):
            for sub_query in self.query:
                ques |= Q(name__icontains=sub_query)
                ques |= Q(desc__icontains=sub_query)
        #execute ques on model
        queryset = Product.get_actives().filter(ques)
        queryset = queryset.order_by(self.get_ordering())
        return queryset


# REST FRAMEWORK views
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import ProductSerializer

class ProductSerializerView(APIView):

    def get_object(self, pk):
        try:
            return Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        product = self.get_object(pk)
        serializer = ProductSerializer(product)
        return Response(serializer.data)

