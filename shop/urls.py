from django.conf.urls import url
from django.contrib.auth.decorators import login_required as lr

from rest_framework.urlpatterns import format_suffix_patterns

from .views import *


urlpatterns = [
    url(r"^$", MainView.as_view(), name='main'),
    url(r"seller/(?P<slug>[-\w\W]+)/$", SellerView.as_view(), name="seller"),
    url(r"products/$", ProductListView.as_view(), name="list"),
    url(r"products/((?P<token>[\w\W]+)/)?$", ProductListView.as_view(), name="deserialize"), #for what is used?
    url(r"keyword/(?P<pk>[\d]+)/$", KeyWordProductListView.as_view(), name="keyword_products"),

    url(r"product/(?P<slug>[\w\W]+)/$", ProductView.as_view(), name="product"),

    url(r"search/$", SearchView.as_view(), name="search"),

    url(r"acceptSpecial/(?P<token>[\w\W]+)/$", lr(AcceptSpecialView.as_view()), name="accept_special"),

    url(r"product_serialize/(?P<pk>[0-9]+)/$", ProductSerializerView.as_view(), name="serialize_product"),
]

urlpatterns = format_suffix_patterns(urlpatterns)