from django.contrib import admin
from .models import *
from .forms import *

x = admin.site.register

class SellerAdmin(admin.ModelAdmin):
    form = SellerAdminForm

    def get_queryset(self, request):
        if request.user.is_admin:
            return Seller.objects.all()
        return Seller.objects.filter(account=request.user)

    def get_fieldsets(self, request, obj=None):
        if request.user.is_admin:
            return (
                (None, {
                    "fields": ("image", "collage_image", "img_height", "img_width", "account", 
                        "idNumber", "shopName", "shopDesc", "slug", "delay",
                        "allow_special"),
                }),
            )
        return (
            (None, {
                "fields": ("image", "idNumber", "shopName", "shopDesc", "allow_special",
                    "delay")
            }),
        )
    
    def get_readonly_fields(self, request, obj=None):
        if request.user.is_admin:
            return ()
        return ("delay", )


class PromotionAdmin(admin.ModelAdmin):
    list_display = ("kind", "discount_kind", "discount_ammount", "start_date", "end_date")
    form = (PromotionAdminForm)
    fieldsets = (
        (None, {
            "fields": ("kind", ("filtered_by", "filter_from", "filter_till"), "products",
                "discount_kind", "discount_ammount", ("start_date", "end_date"),),
        }),
    )

    filter_horizontal = ("products",)

    # def get_queryset(self, request, obj):
    #     if request.user.is_admin:
    #         return Promotion.objects.all()
    #     return Promotion.objects.fitler(owner=request.user)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "products":
            kwargs["queryset"] = Product.objects.filter(owner=request.user)
        return super().formfield_for_manytomany(db_field, request, **kwargs)



x(Seller, SellerAdmin)
x(Promotion, PromotionAdmin)
