from django import forms
from django.core.files.images import get_image_dimensions
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.widgets import FilteredSelectMultiple

from .models import *
from shop.models import Product

ATS = {"class": "form-control", "required": "required"}

class SellerAdminForm(forms.ModelForm):
    IMG_WIDTH_BOUNDS = (250, 2040)
    IMG_HEIGHT_BOUNDS = (250, 520)

    def clean_image(self):
        image = self.cleaned_data.get("image")
        if not image:
            raise forms.ValidationError(_('გთხოვთ შეავსოთ სურათის ველი'))
        width, height = get_image_dimensions(image)

        if width < self.IMG_WIDTH_BOUNDS[0] or width > self.IMG_WIDTH_BOUNDS[1]:
            raise forms.ValidationError(_("სურათის სიგანე უნდა იყოს 1900-დან" +\
                "2300 პიქსელამდე"))
        elif height < self.IMG_HEIGHT_BOUNDS[0] or \
            height > self.IMG_HEIGHT_BOUNDS[1]:
            raise forms.ValidationError(_("სურათის სიმაღლე უნდა იყოს 300-დან" +\
                "500 პიქსელამდე"))
        return image

    class Meta:
        model = Seller
        fields = "__all__"

class SellerForm(forms.ModelForm):
    idNumber = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("პირადი ნომერი"))
    shopName = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("თქვენი გვერდის სახელი"))
    shopDesc = forms.CharField(widget=forms.Textarea(attrs=ATS),
        label=_("თქვენი გვერდის აღწერა"))

    class Meta:
        model = Seller
        fields = ("idNumber", "shopName", "shopDesc")

from django.core.validators import RegexValidator
from authentication.models import Account


class DirectSellerForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs=ATS),
        label=_("ელ.ფოსტა"))
    phone = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("ტელეფონის ნომერი (xxx-xxx-xxx)"), validators=(
        RegexValidator(r"[\d]{3}-[\d]{3}-[\d]{3}", message=_(
        "ნომრის ფორმატი არასწორია")),))
    name = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("სახელი და გვარი"))
    idNumber = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("პირადი ნომერი"))
    shopName = forms.CharField(widget=forms.TextInput(attrs=ATS),
        label=_("თქვენი გვერდის სახელი"))
    shopDesc = forms.CharField(widget=forms.Textarea(attrs=ATS),
        label=_("თქვენი გვერდის აღწერა"))
    password = forms.CharField(widget=forms.PasswordInput(attrs=ATS),
        label=_("პაროლი"))

    def clean_idNumber(self):
        idNumber = self.cleaned_data['idNumber']
        if Seller.objects.filter(idNumber=idNumber).count() > 0:
            raise forms.ValidationError(_("პირადი ნომერი უკვე გამოყენებულია"),
                code="invalid")
        elif len(idNumber) != 11:
            raise forms.ValidationError(_("არასწორი პირადი ნომერი"),
                code="invalid")
        return idNumber

    def clean_shopName(self):
        shopName = self.cleaned_data['shopName']
        if Seller.objects.filter(shopName=shopName).count() > 0:
            raise forms.ValidationError(_("მაღაზიის სახელი დაკავებულია"),
                code="inivalid")
        return shopName

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if Account.objects.filter(phone=phone).count() > 0:
            raise forms.ValidationError(_("ტელეფონის ნომერი უკვე გამოყენებულია"),
                code="invalid")
        return phone

    def clean_name(self):
        name = self.cleaned_data['name']
        if " " not in name:
            raise forms.ValidationError(_("შეიყვანეთ სახელი სწორად"),
                code="invalid")
        return name

    def clean_shopDesc(self):
        desc = self.cleaned_data['shopDesc']
        min_length = 90
        if len(desc) < min_length:
            raise forms.ValidationError(_("გთხოვთ უფრო დეტალურად აღგვიწეროთ" +
            " მაღაზია, {0} სიმბოლოზე მეტად მაინც".format(str(min_length))))
        return desc

    def clean_password(self):
        pswd = self.cleaned_data['password']
        min_length = 6
        if len(pswd) < min_length:
            raise forms.ValidationError(_("პაროლი მოკლეა, შეიყვანეთ " +
            "{0}-ზე მეტი სიმბოლო".format(str(min_length))))
        return pswd


class PromotionAdminForm(forms.ModelForm):
    # products = forms.ModelMultipleChoiceField(queryset=Product.objects.none(),
    #     widget=FilteredSelectMultiple(_("პროდუქტები"), is_stacked=False), required=False)

    class Media:
        js = ("vendor/jquery.min.js", "js/promotionAdmin.js")

    class Meta:
        model=Promotion
        fields="__all__"
    
