from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class SellingConfig(AppConfig):
    name = 'selling'
    verbose_name = _("გაყიდვა")