from django.core.urlresolvers import reverse
from common.models import Tokenizer

def message_to_seller(opk, u):
    token = Tokenizer.get_new_token(obj=True)
    token.update(opk=opk, u=u)
    return reverse("common:m2s", kwargs={"token": token.token})