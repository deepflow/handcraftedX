from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.views import generic
from django.core.urlresolvers import reverse
from django.contrib import messages

from django.utils.translation import ugettext_lazy as _

from shop.models import Product
from authentication.models import Account
from .models import *
from .forms import *

from common.mailer import message_admins

class ExploreSellersView(generic.ListView):
    template_name = 'selling/list.html'
    queryset = Seller.objects.all()

    def get_queryset(self):
        seller_list = []
        for seller in Seller.objects.all():
            if seller.get_item_count() > 0:
                seller_list.append(seller)
        return seller_list

class StartSellingView(generic.TemplateView):
    template_name = "form_renderer.html"

    def get(self, request, *args, **kwargs):
        if Seller.objects.filter(account=request.user).exists() and \
            request.user.is_staff:
            return redirect(reverse("admin:shop_product_changelist"))
        else:
            messages.add_message(self.request, messages.SUCCESS,
                str(_("თქვენი მოთხოვნა გაგზავნილია. პასუხს მიიღებთ ელ.ფოსტაზე")))
            #get user from which request is picked
            account = request.user
            #construct message
            message = _("ახალი მოთხოვნა გამყიდვლის შესახებ:\n{0}|{1}|{2}".format(
                account.email, account.name, account.phone))
            message_admins(message)
            return redirect(reverse("shop:main"))
        return render(request, self.template_name, self.get_context_data(
            **kwargs))

class DirectRegistrationView(generic.TemplateView):
    form_class = DirectSellerForm
    template_name = "selling/direct.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, self.get_context_data(
            form=self.form_class()))

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            account = Account.objects.create(email=form.cleaned_data['email'],
                name=form.cleaned_data['name'],
                phone=form.cleaned_data['phone'])
            account.set_password(form.cleaned_data['password'])
            account.save()
            account.send_activation_mail(self.request.META['HTTP_HOST'])
            #register seller
            Seller.objects.create(idNumber=form.cleaned_data['idNumber'],
                shopName=form.cleaned_data['shopName'],
                shopDesc=form.cleaned_data['shopDesc'],
                account=account)
            #inform admins
            message = _("ახალი მოთხოვნა გამყიდვლის შესახებ:\n{0}|{1}|{2}".format(
                account.email, account.name, account.phone))
            message_admins(message)
            #display success message
            message = _("თქვენ წარმატებით გაიარეთ რეგისტრაცია " +
                "მიჰყევით აკტივაციის ბმულს ელ.ფოსტაზე. ჩვენ კი მზად ვართ " +
                "უახლოეს საათებში განვიხილოთ თქვენი აპლიკაციის ფორმა :)")
            messages.add_message(request, messages.SUCCESS, message)
            return redirect(reverse("selling:direct"))
        return render(request, self.template_name, self.get_context_data(
            form=form))
