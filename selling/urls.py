from django.conf.urls import url
from django.contrib.auth.decorators import login_required as x

from .views import *

urlpatterns = [
    url(r"explore/$", ExploreSellersView.as_view(), name="sellers"),
    url(r"start/$", x(StartSellingView.as_view()), name="start"),
    url(r"directregistration/$", DirectRegistrationView.as_view(), name="direct"),
]
