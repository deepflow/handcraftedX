from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.utils.html import strip_tags
from django.utils import timezone

from common.models import SnapShot
from common.helpers import slugify, generate_token

class Seller(SnapShot):
    image = models.ImageField(verbose_name=_("სურათი"), upload_to="sellers", height_field='img_height', width_field='img_width', null=True, blank=True)
    collage_image = models.ImageField(verbose_name=_("კოლაჟის სურათი"), help_text="გაზიარდება fb-ზე", blank=True, null=True)
    img_height = models.PositiveIntegerField(blank=True, null=True)
    img_width = models.PositiveIntegerField(blank=True, null=True)

    account = models.OneToOneField("authentication.Account", related_name="seller", unique=True)
    idNumber = models.CharField(_("პირადი ნომერი"), max_length=11, unique=True)
    shopName = models.CharField(_("გვერდის სახელი"), max_length=64)
    shopDesc = models.TextField(_("გვერდის აღწერა"), blank=True)

    slug = models.CharField(_("სლაგი (ავტ)"), max_length=128, default=generate_token)

    allow_special = models.BooleanField(_("გაკეთდეს სპეციალური შეთავაზებები"), default=False, blank=True)
    delay = models.PositiveIntegerField(_("მიწოდების პერიოდი (საათი)"), default=24,
        help_text=_("პროდუქტის შეკვეთიდან, დრო, რომლის განმავლობაშიც პროდუქტი უნდა მომზადდეს ტრანსპორტირებისთვის"))

    @property
    def short_desc(self):
        return " ".join(strip_tags(self.shopDesc).split(" ")[:10]) + "..."

    def get_absolute_url(self):
        return reverse("shop:seller", kwargs={"slug":self.slug})

    def get_item_count(self):
        return self.account.get_seller_item_count()

    def __str__(self):
        return "{0} {1}".format(str(self.account), self.shopName)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.shopName)
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("პროდუქტის განმთავსებელი")
        verbose_name_plural = _("პროდუქტის განმთავსებლები")
        ordering = ["created"]


class Promotion(SnapShot):
    PKC = (
        ("0", _("ყველა პროდუქტზე")),
        ("1", _("კონკრეტულ პროდუქტებზე")),
        ("2", _("გაფილტრულ პროდუქტებზე")),
    )
    DKC = (
        ("0", _("პროცენტი (%)")),
        ("1", _("თანხა (GEL)")),
    )
    FBC = (
        ("0", _("ფასის მიხედვით")),
    )

    kind = models.CharField(_("ტიპი"), max_length=1, default="0", choices=PKC)
    filtered_by = models.CharField(_("გაფილტრული"), max_length=1, default="0", choices=FBC, blank=True)
    products = models.ManyToManyField("shop.Product", verbose_name=_("პროდუქტები"), blank=True)
    discount_kind = models.CharField(_("ფასდაკლების ტიპი"), max_length=1, default="0", choices=DKC, blank=True)
    discount_ammount = models.DecimalField(_("ფასდაკლების რაოდენობა"), decimal_places=2, max_digits=5, default=0.00, blank=True)

    start_date = models.DateTimeField(_("დაწყების თარიღი"), default=timezone.now)
    end_date = models.DateTimeField(_("დასრულების დრო"), default=timezone.now)
    filter_from = models.DecimalField(_("დან (ფასი)"), max_digits=5, decimal_places=2, blank=True, default=0.00)
    filter_till = models.DecimalField(_("მდე (ფასი)"), max_digits=5, decimal_places=2, blank=True, default=1.00)


    def __str__(self):
        return self.get_kind_display()

    class Meta:
        verbose_name = _("ფასდაკლება")
        verbose_name_plural = _("ფასდაკლებები")
