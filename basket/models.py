from django.db import models
from django.utils.translation import ugettext_lazy as _
from decimal import Decimal as D
from django.core import serializers
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.contrib.sessions.models import Session

from shop.models import ProductOption
from common.models import SnapShot

import json

class Line(models.Model):
    option = models.ForeignKey("shop.ProductOption", verbose_name=_("პროდუქტი"), blank=True, null=True)
    quantity = models.IntegerField(_("რაოდენობა"), default=1)

    def freeze(self):
        """
            line freezer, acts as basket's one and serves it
            freeze() -> dict / json
        """
        data = {"quantity": self.quantity, "price": str(self.option.price),
            "tax": str(self.option.tax), "id": str(self.option.pk)}
        return data

    def get_price(self):
        '''
            returns lines total price
            get_price() -> Decimal
        '''
        return self.option.get_price() * self.quantity

    def __str__(self):
        return str(self.option)

    class Meta:
        verbose_name = _("კალათის ხაზი")
        verbose_name_plural = _("კალათის ხაზები")


class Basket(SnapShot):
    lines = models.ManyToManyField(Line, verbose_name=_("კალათის ხაზები"), blank=True)
    session = models.ForeignKey(Session, verbose_name=_("სესია"), blank=True, null=True)

    def freeze(self):
        '''
            freezes basket object for order information, in case
            option changes and other manipulations
            freeze() -> dict / json
        '''
        return [x.freeze() for x in self.lines.all()]

    def is_empty(self):
        if self.lines.count():
            return False
        return True

    def get_origin_count(self):
        '''returns count of the different sellers in basket'''
        try:
            options = [l.option.get_seller().pk for l in self.lines.all()]
            return len(set(options))
        except Exception as ge:
            return 1

    def get_delivery_price(self):
        DEL_PRICE = D(3.00)
        return DEL_PRICE * D(self.get_origin_count())

    def get_total(self):
        price = D(0.00)
        for line in self.lines.all():
            price += line.get_price()
        # price += D(3.00) #Delivery service
        return price
    
    def add_line(self, o, q):
        '''
            adds/updates line to basket, based on option object and quantity
            add_line(ProductOption, int) -> bool
        '''
        if not o.is_available(q):
            return False
        line = self.lines.filter(option=o)
        if line.count():
            return self.update_line(o, q)
        else:
            self.lines.add(Line.objects.create(option=o, quantity=q))
            return True
    
    def rm_line(self, o):
        '''
            removes line based on option from self.lines
            rm_lin(ProductOption) -> bool
        '''
        try:
            line = self.lines.get(option=o)
            line.delete()
            return True
        except Line.DoesNotExist:
            return False

    def update_line(self, o, q):
        '''
            updates line quantity based option object and new quantity
            update_line(int) -> bool
        '''
        try:
            line = self.lines.get(option__pk=o.pk)
            total_quantity = line.quantity + q
            if line.option.is_available(total_quantity):
                line.quantity = total_quantity
            else:
                line.quantity = line.option.quantity
            line.save()
            return True
        except Line.DoesNotExist:
            return False

    def update_line_quantity(self, o, q):
        '''
            updates line quantity relatively based on option object and new quantity
        '''
        try:
            line = self.lines.get(option__pk=o.pk)
            if line.option.is_available(q):
                line.quantity = q
                line.save()
                return True
            return False
        except Line.DoesNotExist:
            return False

    def __str__(self):
        return "კალათა N{0}".format(self.pk)

    # STATIC METHODS
    @staticmethod
    def sadd_option(bpk, opk, quantity):
        '''
            static method for adding product option in basket
            sadd_option(int, int, int) -> str
        '''
        try:
            basket = Basket.objects.get(pk=bpk)
            option = ProductOption.objects.get(pk=opk)
            if basket.add_line(option, quantity):
                return _("პროდუქტი წარმატებით დავამატე კალათაში")
            return _("პროდუქტის რაოდენობა ამოწურულია, უნდა გეჩქარა")
        except Basket.DoesNotExist:
            return _("შენს კალათას ვერ ვპოულობ, ხომ არ დაკარგე?!")
        except ProductOption.DoesNotExist:
            return _("პროდუქტს ვერ ვპოულობ, ხომ არ გეშლება?!")
    
    @staticmethod
    def srm_option(bpk, opk):
        '''
            static method for removing product option from basket
            srm_option(int, int) -> str
        '''
        try:
            basket = Basket.objects.get(pk=bpk)
            option = ProductOption.objects.get(pk=opk)
            basket.rm_line(option)
            return _("პროდუქტი წარმატებით წავშალე კალათიდან")
        except Basket.DoesNotExist:
            return _("შენს კალათას ვერ ვპოულობ, ხომ არ დაკარგე?!")
        except ProductOption.DoesNotExist:
            return _("პროდუქტს ვერ ვპოულობ, ხომ არ გეშლება?!")

    def supdate_quantity(bpk, opk, newQ):
        '''
            static method for updating basket's option's line quantity
            supdate_quantity(int, int, int) -> dict
        '''
        data = {"message": None}
        try:
            basket = Basket.objects.get(pk=bpk)
            option = ProductOption.objects.get(pk=opk)
            if not basket.update_line_quantity(option, newQ):
                data['message'] = str(_("რაოდენობა არასაკმარისია, სამწუხაროდ :("))
            line = basket.lines.get(option__pk=opk)
            data['line_total'] = str(line.get_price())
            data['basket_total'] = str(basket.get_total())
            data['quantity'] = line.quantity

        except Basket.DoesNotExist:
            data['message'] = str(_("შენს კალათას ვერ ვპოულობ, ხომ არ დაკარგე?!"))
        except ProductOption.DoesNotExist:
            data['message'] = str(_("პროდუქტს ვერ ვპოულობ, ხომ არ გეშლება?!"))
        except Line.DoesNotExist:
            data['message'] = str(_("პროდუქტს ვერ ვპოულობ, ხომ არ გეშლება?!"))
        data = json.dumps(data)
        return data

    class Meta:
        verbose_name = _("კალათა")
        verbose_name_plural = _("კალათები")
        ordering = ('-created', )


@receiver(pre_delete, sender=Basket)
def basket_pre_delete_handler(sender, instance, **kwargs):
    # ensure that all lines for basket are deleted before basket deletion
    instance.lines.all().delete()