from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class BasketConfig(AppConfig):
    name = 'basket'
    verbose_name = _("კალათა")
