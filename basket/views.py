from django.shortcuts import render
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from .models import Basket
from shop.models import ProductOption

class BasketCoreOps:
    def get_basket(self):
        return Basket.objects.get_or_create(
            account=self.request.user)[0]

    def get_option(self, opk):
        try:
            return ProductOption.objects.get(pk=opk)
        except Product.DoesNotExist:
            return None

    def goto_referer(self):
        return redirect(self.request.META['HTTP_REFERER'])

class AddView(generic.RedirectView, BasketCoreOps):
    def get(self, request, *args, **kwargs):
        object = self.get_basket()
        option = self.get_option(kwargs.get("opk"))
        count = int(kwargs.get("cnt"))

        if option is None:
            messages.add_message(request, messages.WARNING,
                _("სამწუხაროდ, პროდუქტი ვერ დავამატე :("))
            return self.goto_referer()
        if object.add_line(option, count) is False:
            messages.add_message(request, messages.WARNING,
                _("სამწუხაროდ, პროდუქტის რაოდენობა არასაკმარისია :("))
            return self.goto_referer()
        messages.add_message(request, messages.SUCCESS,
            _("პროდუქტი კალათაში წარმატებით დავამატე"))
        return self.goto_referer()

class RemoveView(generic.RedirectView, BasketCoreOps):
    def get(self, request, *args, **kwargs):
        object = self.get_basket()
        option = self.get_option(kwargs.get("opk"))

        if option is None:
            messages.add_message(request, messages.WARNING,
                _("პროდუქტი ვერ წავშალე :("))
            return self.goto_referer()
        object.rm_line(option)
        messages.add_message(request, messages.SUCCESS,
            _("პროდუქტი წარმატებით წავშალე კალათიდან :)"))
        return self.goto_referer()


class BasketView(generic.TemplateView):
    template_name = "basket/basket.html"
    pass
