from django.conf.urls import url
from django.contrib.auth.decorators import login_required as x

from .views import *

urlpatterns = [
    url(r"basket/$", BasketView.as_view(), name="basket"), # basket view for unauthed users

    url(r"add/(?P<opk>[\d]+)/(?P<cnt>[\d]+)/$", x(AddView.as_view()), name="add"),
    url(r"rm/(?P<opk>[\d]+)/$", x(RemoveView.as_view()), name="rm"),
]
