import json
from django import forms
from .models import Address, Country, City

ATB = {'class':'form-control'}

def get_country_delivery_prices():
    data = {str(c.pk): float(c.delivery_price) for c in Country.objects.all()}
    return json.dumps(data)

def get_city_delivery_prices():
    data = {str(c.pk): float(c.delivery_price) for c in City.objects.all()}
    return json.dumps(data)

class AddressForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for k, v in self.fields.items():
            if k == 'lng' or k == 'lat':
                v.widget = forms.HiddenInput()
            else:
                v.widget.attrs.update(ATB)
        self.fields["country_meta"].initial = get_country_delivery_prices()
        self.fields["city_meta"].initial = get_city_delivery_prices()

    country_meta = forms.CharField(widget=forms.HiddenInput())
    city_meta = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Address
        fields = "__all__"

    class Media:
        js = ("js/address.js", )
