import re
from django.http import HttpResponse
from decimal import Decimal as D

from authentication.models import EmailAutomation
from .models import Order


def dist_success_emails(order):
    try:
        order.owner.notify_successful_order()
        EmailAutomation.notify_order(order, state="წარმატებული")
    except Exception as ge:
        pass

def dist_fail_emails(order):
    try:
        EmailAutomation.notify_order(order, state="წარუმატებელი")
    except Exception as ge:
        pass

def get_node_value(node_name, data):
    """
        function to retrieve node value in XML text
        get_node_value(str, str) -> str
    """
    pattern = r"<{0}>[-\w\W]+</{0}>".format(node_name)
    match = re.findall(pattern, data)
    if not len(match):
        return None
    match = match[0]
    match = match.replace("<{0}>".format(node_name), "")
    match = match.replace("</{0}>".format(node_name), "")
    match = match.replace(" ", "")
    return str(match)

def response(trans_id, payment_id, token, status=False):
    """
        function constructs XML and returns populated
        HttpResponse
    """
    flag = "ACCEPTED" if status else "DECLINED"
    xml = """
        <ConfirmResponse>
            <TransactionId>{0}</TransactionId>
            <PaymentId>{1}</PaymentId>
            <Status>{2}</Status>
        </ConfirmResponse>
    """.format(trans_id, payment_id, flag)
    token.update(xml=xml)
    return HttpResponse(xml, content_type="text/xml")

def extract_ammount(amnt):
    """
        ammount format is without any dots, so
        it should be converted or not
    """
    if not "." in amnt:
        return D(amnt[:-2] + "." + amnt[-2:])
    return amnt

def check_transaction(tid, amnt, success, token):
    """
        checks transaction based on tid and amnt, if success
        then modifies transaction accordingly
        check_transaction(int, str, bool) -> bool
    """
    try:
        amnt = str(amnt)[:-2] + "." + str(amnt)[-2:]
        # check if order exists @ all
        try:
            order = Order.objects.get(pk=tid)
            if str(amnt) == str(order.get_price()):
                token.update(price_check=True)
                if success:
                    token.update(order_success=True)
                    order.confirm_order()
                    # in addition notify user and admins that order is received
                    dist_success_emails(order)
                # on order check, return order validation data
                return order.is_valid() # to reassure the validity of the order
            dist_fail_emails(order)
            return False
        except Order.DoesNotExist:
            # if not return False
            token.update(no_order=True)
            return False
    except Exception as ge:
        token.update(exception=ge)
        return False
