from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import get_language
from common.models import SnapShot
from authentication.models import Account

from decimal import Decimal as D

class Country(models.Model):
    name_ka = models.CharField(_("სახელი (ქართ)"), max_length=32)
    name_en = models.CharField(_("სახელი (ინგ)"), max_length=32)
    name_ru = models.CharField(_("სახელი (რუს)"), max_length=32, blank=True)
    delivery_price = models.DecimalField(_("მიწოდების ფასი"), decimal_places=2, max_digits=5, blank=True, default=D(0.00))

    @property
    def name(self):
        fname = "name_{0}".format(get_language())
        if hasattr(self, fname):
            return getattr(self, fname)
        return self.name_ka

    def get_children_admin(self):
        return " | ".join([str(x) for x in self.city_set.all()])
    get_children_admin.short_description = _("შვილობილი ობიექტები")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("ქვეყანა")
        verbose_name_plural = _("ქვეყნები")


class City(models.Model):
    country = models.ForeignKey(Country, verbose_name=_("ქვეყანა"), blank=False)
    name_ka = models.CharField(_("სახელი (ქართ)"), max_length=32)
    name_en = models.CharField(_("სახელი (ინგ)"), max_length=32)
    name_ru = models.CharField(_("სახელი (რუს)"), max_length=32, blank=True)
    delivery_price = models.DecimalField(_("მიწოდების ფასი"), help_text=_("ყოველთვის იქნება >= მშობელი ქვეყნის ფასზე"), 
        decimal_places=2, max_digits=5, blank=True, default=D(0.00))

    @property
    def name(self):
        fname = "name_{0}".format(get_language())
        if hasattr(self, fname):
            return getattr(self, fname)
        return self.name_ka

    def get_parent(self):
        return str(self.country)
    get_parent.short_description = _("მშობელი ქვეყანა")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("ქალაქი")
        verbose_name_plural = _("ქალაქები")


class Address(models.Model):
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, verbose_name=_("ქვეყანა"), null=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, verbose_name=_("ქალაქი"), null=True)
    line1 = models.CharField(_("უბანი * "), max_length=56)
    line2 = models.CharField(_("ქუჩა * "), max_length=128)
    line3 = models.CharField(_("ზუსტი მისამართი"), max_length=128, blank=True)

    lng = models.CharField(_("გრძედი"), max_length=128, blank=True)
    lat = models.CharField(_("განედი"), max_length=128, blank=True)

    phone = models.CharField(_("ტელეფონის ნომერი * "), max_length=19)
    message = models.CharField(_("დამატებითი შეტყობინება"), max_length=256, blank=True)

    def get_delivery_price(self):
        """
            delivery price is calculated based on country and city, like
            the price is returned based on which price is greater than
        """
        # depricated - never will occure
        # if self.country.delivery_price > self.city.delivery_price:
        #     return self.country.delivery_price
        # return self.city.delivery_price
        return self.city.delivery_price

    def get_owner(self):
        return self.account_set.first()
    get_owner.short_description = _("მისამართის პატრონი")

    def __str__(self):
        return "{0} - {1}".format(self.line1, self.line2)

    class Meta:
        verbose_name = _("მისამართი")
        verbose_name_plural = _("მისამართები")


class Order(SnapShot):
    OSC = (
        # ("u", _("დასაზუსტებელია")),
        # ("n", _("დაუარებულია (ბანკის მიერ)")),
        ("0", _("მუშავდება")),
        ("1", _("გზაშია")),
        ("2", _("მიტანილია"))
    )

    lines = models.ManyToManyField("basket.Line", blank=True)
    status = models.CharField(_("შეკვეთის სტატუსი"), max_length=1, default='0', choices=OSC)
    owner = models.ForeignKey("authentication.Account", verbose_name=_("შემკვეთი"), blank=True)
    # price calculation is bound to post_save signal (below)
    price = models.DecimalField(_("შეკვეთის ფასი"), decimal_places=2, max_digits=6, blank=True, default=0.00)
    deliveryPrice = models.DecimalField(_("მიწოდების ფასი"), decimal_places=2, max_digits=6, blank=True, default=0.00)
    # detailed information about order
    meta = models.TextField(_("შეკვეთის მეტა"), blank=True)

    def confirm_order(self):
        """
            once money transaction is confirmed from bank
            flush user basket and update product quantities
            confirm_order(Basket) -> None
        """
        for line in self.lines.all():
            self.owner.basket.lines.remove(line)
            line.option.quantity -= line.quantity
            line.option.save()
        self.status = "1"
        self.save()

    def is_valid(self):
        """
            right before purchase confirm that product
            quantities are available
            is_valid() -> bool
        """
        truth_table = []
        for line in self.lines.all():
            truth_table.append(
                line.option.is_available(line.quantity))
        return sum(truth_table) == len(truth_table)

    def get_price(self):
        # price is calculated like - order product price + delivery price
        return self.price + self.deliveryPrice

    def __str__(self):
        return self.get_status_display()

    class Meta:
        verbose_name = _("შეკვეთა")
        verbose_name_plural = _("შეკვეთები")
        ordering = ['-created']


#SIGNALS
from django.db.models.signals import post_save
from django.dispatch import receiver

# AUTOMATIC PRICE CALCULATION AFTER SAVING ORDER
@receiver(signal=post_save, sender=Order)
def order_post_save(sender, **kwargs):
    instance = kwargs.get("instance", None)
    created = kwargs.get("created", None)
    if instance is not None and instance.price == 0.00:
        instance.price = sum([line.get_price() for line in instance.lines.all()])
    if created: # in case intance is created make sure to send email
        Account.notify_order(instance, state="შექმნა")

@receiver(signal=post_save, sender=City)
def city_post_save(sender, created, instance, **kwargs):
    # make sure that city's delivery price won't be less than country's
    if instance.delivery_price < instance.country.delivery_price:
        instance.delivery_price = instance.country.delivery_price
        instance.save()

