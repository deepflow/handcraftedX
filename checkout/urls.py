from django.conf.urls import url
from django.contrib.auth.decorators import login_required as x

from .views import *

urlpatterns = [
    url(r"purchase-success/(?P<order_pk>[\d]+)/$", x(PurchaseSuccessView.as_view()), name="purchase_success"),
    url(r"purchase-fail/(?P<order_pk>[\d]+)/$", x(PurchaseFailView.as_view()), name="purchase_fail"),
    url(r"sumup/(?P<pk>[\d]+)/$", x(PurchaseView.as_view()), name="sumup"),
    url(r"addressAdd/$", x(CreateAddressView.as_view()), name="address_add"),
    url(r"update/(?P<pk>[\d]+)/$", x(UpdateAddressView.as_view()), name="update"),

    url(r"service/address/$", x(AddressView.as_view()), name='address_service'),

    # Cartu bank back check url
    url(r"check_purchase/$", check_purchase, name="check_purchase"),
]
