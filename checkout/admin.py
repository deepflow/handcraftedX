from django.contrib import admin
from .models import Address, Order, Country, City

x = admin.site.register


class OrderAdmin(admin.ModelAdmin):
    list_display = ("owner", "status", "price", )


class CityAdminInline(admin.StackedInline):
    model = City
    fields = (("name_ka", "name_en", "name_ru", "delivery_price"), )


class CityAdmin(admin.ModelAdmin):
    fields = (("name_ka", "name_en", "name_ru", "delivery_price"), )
    list_display = ("__str__", "get_parent", "delivery_price")


class CountryAdmin(admin.ModelAdmin):
    inlines = (CityAdminInline, )
    list_display = ("__str__", "get_children_admin", "delivery_price")
    fields = (("name_ka", "name_en", "name_ru", "delivery_price"), )


class AddressAdmin(admin.ModelAdmin):
    list_display = ("get_owner", "country", "city", "phone")


x(Address)
x(Order, OrderAdmin)
x(Country, CountryAdmin)
x(City, CityAdmin)