from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class CheckoutConfig(AppConfig):
    name = 'checkout'
    verbose_name = _("სალარო კონტროლი")