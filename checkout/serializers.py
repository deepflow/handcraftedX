from rest_framework import serializers
from .models import Country, City

class CitySerializer(serializers.ModelSerializer):
    searchable = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    def get_searchable(self, obj):
        return "".join([obj.name_en, obj.name_ka, obj.name_ru])

    def name(self, obj):
        return obj.name

    class Meta:
        model = City
        fields = ("pk", "name_ka", "name_en", "name_ru", "searchable", "name")


class CountrySerializer(serializers.ModelSerializer):
    city_set = CitySerializer(read_only=True, many=True)
    searchable = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()

    def get_searchable(self, obj):
        return "".join([obj.name_en, obj.name_ka, obj.name_ru])

    def get_name(self, obj):
        return obj.name

    class Meta:
        model = Country
        fields = ("pk", "name_ka", "name_en", "name_ru", "city_set", "searchable", "name")

