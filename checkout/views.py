from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.views import generic
from django.http import HttpResponse
from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages

import json
from decimal import Decimal as D

from common.models import Tokenizer
from .models import Address, Order, Country, City
from .forms import AddressForm
from .helpers import *
from .serializers import CountrySerializer

class PurchaseView(generic.TemplateView):
    template_name = "checkout/sumup.html"

    def get_object(self):
        return get_object_or_404(Order, pk=self.kwargs.get("pk", None))

    def get(self, request, *args, **kwargs):
        if request.user.basket.lines.count() == 0:
            messages.add_message(request, messages.WARNING,
                _("პროდუქტები არ არის კალათაში, დაამატე"))
            return redirect("shop:list")
        return render(request, self.template_name,
            self.get_context_data(order=self.get_object()))

class CreateAddressView(generic.CreateView):
    model = Address
    form_class = AddressForm
    template_name = "checkout/address.html"
    object = None

    def form_valid(self, form):
        entry = form.save()
        self.request.user.address = entry
        self.request.user.save()
        messages.add_message(self.request, messages.SUCCESS,
            _("მისამართი წარმატებით დავაფიქსირე"))
        # craete order here and redirect user
        basket = self.request.user.basket
        order = Order(owner=self.request.user)
        order.price = basket.get_total()
        order.meta = basket.freeze()
        # delivery price was retrieved from basket - deprica ted
        # order.deliveryPrice = basket.get_delivery_price()
        # now delivery price is retrieved from address - needs update based on basket
        order.deliveryPrice = entry.get_delivery_price()
        order.save()
        order.lines.add(*basket.lines.all())
        return redirect(reverse("checkout:sumup", kwargs={"pk": order.pk}))

    def get(self, request, *args, **kwargs):
        # if request.user.address is None:
        return render(request, self.template_name, self.get_context_data(
            form=AddressForm(instance=request.user.address)))

class UpdateAddressView(generic.UpdateView):
    model = Address
    form_class = AddressForm
    template_name = "form_renderer.html"

    def form_valid(self, form):
        form.save()
        messages.add_message(self.request, messages.SUCCESS,
            _("მისამართი წარმატებით შევცვალე"))
        return redirect(reverse("auth:profile"))

    def get_object(self):
        return Address.objects.get(pk=self.kwargs.get("pk", 0))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = _("მისამართის განახლება")
        context['formTitle'] = _("განაახლე მისამართი")
        context['formUrl'] = reverse("checkout:update",
            kwargs={'pk':self.object.pk})
        context['formText'] = _("განახლება")
        context['include'] = True
        context['includeTemplate'] = 'partials/map.html'
        return context

#METHOD SHOULD BE REDESIGNED and REMODIFIED
class PurchaseSuccessView(generic.DetailView):
    template_name = "checkout/purchase_success.html"
    object = None

    def get_object(self):
        return get_object_or_404(Order, pk=self.kwargs.get("order_pk"))

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if not self.object.is_valid():
            return redirect(reverse("checkout:purchase_fail", kwargs={"order_pk": self.object.pk}))
        #!!!move this code to bank callback url!!!
        self.object.confirm_order(request.user.basket)
        #!!!move this code to bank callback url!!!
        return render(request, self.template_name, self.get_context_data(**kwargs))

class PurchaseFailView(generic.TemplateView):
    template_name = "checkout/purchase_fail.html"


class AddressView(generic.TemplateView):
    
    def get(self, request, *args, **kwargs):
        serialized = CountrySerializer(Country.objects.all(), many=True)
        return JsonResponse(serialized.data, safe=False)


@csrf_exempt
def check_purchase(request):
    if request.method == "POST":
        token = Tokenizer.get_new_token(obj=True)
        CHECK_STATUS = (chr(1057), chr(67)) # C
        SUCCSS_STATUS = (chr(89), chr(1059)) # Y
        # get request main data as XML
        data = request.POST.get("ConfirmRequest", None)
        # if data is not present deny purchase request
        if not data:
            return response(0, 0, False)
        tid = get_node_value("TransactionId", data) # transaction id
        pid = get_node_value("PaymentId", data) # payment id
        ammount = get_node_value("Amount", data) # transaction ammount
        status = get_node_value("Status", data) # get check request status

        token.update(code_number=ord(status))

        # log each value to token
        token.update(trans_id=tid, payment_id=pid, price=ammount,
            status=status)

        # if request came to check if transaction exists and info matches
        if status in CHECK_STATUS and check_transaction(tid, ammount, False, token):
            token.update(request="checking")
            resp = response(tid, pid, token, True)
            token.update(response=resp)
            return resp
        # else if request status is success recheck everything
        elif status in SUCCSS_STATUS and check_transaction(tid, ammount, True, token):
            token.update(request="success")
            resp = response(tid, pid, token, True)
            token.update(response=resp)
            return resp
        # other case is not defined so return bad request code
        token.update(request="can't understand")
        resp = response("1", "1", token, False)
        token.update(response=resp)
        return resp
    return HttpResponse("ok")
