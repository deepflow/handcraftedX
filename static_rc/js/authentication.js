$(document).ready(function(){
    $("#id_phone").mask("999-999-999");
    $("#signinForm").submit(handleSignin);
    $("#signupForm").submit(handleSignup);
    
    if (window.location.href.indexOf("#signin") != -1)
        $("#authModal").modal("show");
    if (window.location.href.indexOf("#signup") != -1){
        $("#authModal").modal("show");
        $("a[href=#signup]").click();
    }
});

function handleSignup(e){
    e.preventDefault();
    var url = $("#signupForm").attr("action");
    var formData = $("#signupForm").serialize();
    $.post(url, formData, function(data, status){
        if (data["redirect"])
            window.location.href = data["redirect"];
        $.each(JSON.parse(data), function(k, v){
            $("#signupErrors").append(v[0]["message"] + "<br>");
        });
    });
}

//---------- NATIVE AUTH LOGIC ----------
function handleSignin(event){
    event.preventDefault();
    var formData = $(event.target).serialize();
    var url = $(event.target).attr("action");
    $.post(url, formData, function(data, status){
        if (data['redirect'])
            window.location.href = data['redirect'];
        else if (data['error'])
            $("#signinError").text(data['error']);
    });
}


// ---------- FB AUTH LOGIC ----------
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        if (response.status == "connected"){
            FB.api('/me?fields=email,name', function(response) {
            if ("name" in response && "email" in response && "id" in response){
                $.get("/auth/socauth/", {
                email: response.email,
                name: response.name,
                id: response.id}, function(data, status){
                    if (data["redirect"])
                        window.location.href = data['redirect']
                });
            }
            });
        } else {
            // TODO SOME ERROR
        }
    });
}

