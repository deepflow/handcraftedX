/**
 * if page is loaded in shop/search/ url, it automatically hanldes
 * ordering issues on the page
 */
// $(document).ready(function(){
//   if ($("#search_form").length){
//     // handle ordering assignment on page
//     $("[name='ordering']").val(get_param_byname("ordering"));
//   }
// });

function inject_param(element){
  var url = pre_inject_param(element);
  window.location.href = url;
}

function pre_inject_param(element){
  var qname = $(element).attr("data-qname");
  var qvalue = $(element).attr("data-value");
  console.log(qname, qvalue)
  var url = window.location.href;
  if (url.indexOf(qname) > 0){
    url = url.replace(qname + "=" + get_param_byname(qname), qname + "=" + qvalue);
  } else {
    //Check if url contains query string already
    if (url.indexOf("?") > 0)
      url = url + "&" + qname + "=" + qvalue;
    else
      url = url + "?" + qname + "=" + qvalue;
  }
  return url
}

function get_param_byname(name) {
  url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

