function ps(pk){
    this.serviceUrl = "/store/product_serialize/PK/";
    $.get(this.serviceUrl.replace("PK", pk), function(data, status){
        psOnReady(data);
    });
}

function psOnReady(data){
    var SHTML = ['<li><a data-slide-index="DSI" href=""><img alt="" class="img-responsive" src="ISRC"></a></li>',
        '<li><img alt="" class="img-responsive" src="ISRC"></li>', '<option value="OVL" data-q="OQT">OTX</option>']
    $.each(data['images'], function(i, url){
        $("#slider-thumbs").append(SHTML[0].replace("ISRC", url).replace("DSI", i));
        $("#slider1").append(SHTML[1].replace("ISRC", url));
    });
    $.each(data['options'], function(i, o){
        $("#optionSelector").append(SHTML[2].replace("OVL", o.pk)
            .replace("OQT", o.quant).replace("OTX", o.name + " - " + o.price));
    });
    $.each(data['keywords'], function(i, k){
        $("#keywords").append('<a href="' + k.url + '">' + k.name + '</a> ');
    });
    $("#quickview-name").html(data['name']);
    $("#quickview-desc").html(data['desc']);
    $("#price-range").html(data['price_range']);

    $("#optionSelector").on("change", function(){
        set_arg("#basketAddBtn", "opk", $("#optionSelector").val());
    });
    $("#optionSelector").trigger("change");
    set_arg("#quickview-upvote", "ppk", data['pk']);

    $("#quickview").modal("show");
    $(document).on('hide.bs.modal','#quickview', psOnClose);
}

function psOnClose(){
    $("#slider-thumbs").html("");
    $("#slider1").html("");
    $("#qucikview-desc").html("");
    $("#optionSelector").html("");
    $("#price-range").html();
    $("#keywords").html("");
}