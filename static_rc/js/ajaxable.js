function ajaxable_v2(event, callback=null){
  event.preventDefault();
  var target = event.currentTarget;
  var serviceUrl = '/ajaxable/proto/FNAME-ARGS'.replace(
    "FNAME", $(target).attr("data-method")).replace(
    "ARGS", $(target).attr("data-args"));
  console.log(serviceUrl);
  $.get(serviceUrl, function(data, status){
    var result = data['result'];
    if (result != 'error' && callback)
      callback(result, target);
    else if (result != 'error' && !callback){
      show_message_v2(result);
      console.log("call successful, result=", result);
    }
    else
      console.log("call ended with error", result);
  });
}

function ajaxable_v2_wrap(target, callback=null){
  event.preventDefault();
  var serviceUrl = '/ajaxable/proto/FNAME-ARGS'.replace(
    "FNAME", $(target).attr("data-method")).replace(
    "ARGS", $(target).attr("data-args"));
  console.log(serviceUrl);
  $.get(serviceUrl, function(data, status){
    var result = data['result'];
    if (result != 'error' && callback)
      callback(result, target);
    else if (result != 'error' && !callback){
      show_message_v2(result);
      console.log("call successful, result=", result);
    }
    else
      console.log("call ended with error", result);
  });
}

function show_message_v2(message){
  // $("#message_ajaxable").html(message);
  // console.log($(".bs-ajaxable-modal-sm"));
  $(".bs-ajaxable-modal-sm").modal("show");
  $("#ajaxable-modal-content").html(message);
  setTimeout(hide_messsage(), 2000);
}

function hide_messsage(){
  $("#ajaxable-modal-content").modal("hide");
  $("#ajaxable-modal-content").html(''); //clean last message
}

// get_args("#selector") - sample
function get_args(selector){
  // returns args of ajaxable element
  var args = $(selector).attr("data-args").replace(/ /g, '');
  return JSON.parse(args);
}

// set_arg("#m2sBtn", "opk", $("#optionSelector").find(":selected").val()); - sample
function set_arg(selector, aname, aval){
  // changes arg and resets to selector found element
  var args = get_args(selector);
  args[aname] = aval;
  $(selector).attr("data-args", JSON.stringify(args));
}





// DEPRICATION IN PROGRESS
function ajaxable(fname, event, callback){
  var target = event.currentTarget
  var URL = "/ajaxable/proto/FNAME-ARGS".replace("FNAME", fname).replace(
    "ARGS", $(target).attr("args"));
  console.log(URL)
  $.get(URL, function(data, status){
    var result = data['result'];
    if (result != 'error')
      callback(result, target);
    else
      console.log("error caused - ", result)
  });
}

function show_message(message){
  $(".message-ajaxable-hide").addClass("message-ajaxable");
  $(".message-ajaxable-hide").removeClass("message-ajaxable-hide");
  $(".message-content").html(message);
  setTimeout(hide_messsage, 3000);
}

function hide_messsage(){
  $(".message-ajaxable").addClass("message-ajaxable-hide");
  $(".message-ajaxable").removeClass("message-ajaxable");
}