var animationTime = 400;

$(document).ready(function(){

    // attach wiki link
    var initial = $(".module.aligned").first().find("h2").text();
    initial += '<span style="padding-left:20px;"></span><a style="color:yellow;font-weight:600;" href="http://handx.org/hiw/" target="_blank">როგორ მუშაობს?</a>'
    $(".module.aligned").first().find("h2").html(initial);

    $("#id_visible").on("change", function(){
        if (!$("#id_visible").is(":checked"))
            $(".field-box.field-publish_at").hide(animationTime);
        else
            $(".field-box.field-publish_at").show(animationTime);
    });
    
    $("#id_share").on("change", function(){
        if (!$("#id_share").is(":checked"))
            $(".field-box.field-share_at").hide(animationTime);
        else
            $(".field-box.field-share_at").show(animationTime);
    });

    $("#id_share").trigger("change");
    $("#id_visible").trigger("change");

});