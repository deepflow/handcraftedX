$(document).ready(function(){
    hideFields();
    initWorkspace();
    // attach event listeners
    $("#id_kind").on("change", kindChangeHandler);
    $("#id_products").on("change", productSelectHandler);
});

function initWorkspace(){
    $(".submit-row").before('<div class="module" id="workspace"><h2><strong>კალიფსო</strong></h2><br></div>');
    $("#workspace").append('<p id="short_info">ფასდაკლება მოიცავს <span id="pcount">0</span> პროდუქტს</p>');
}

function hideFields(easy=false){
    var hideables = [".field-filtered_by", ".field-products"]
    $.each(hideables, function(i, s){
        if (easy)
            $(s).fadeOut();
        else
            $(s).hide();
    });
}

function kindChangeHandler(){
    hideFields(true);
    var s = "#id_kind";
    var val = $(s).val();
    if (val == "0"){
        $("#pcount").html($("#id_products").length);
    }
    else if (val == "1"){
        console.log("hasdas")
        $(".field-products").fadeIn();
    } else if (val == "2"){
        $(".field-filtered_by").fadeIn();
    }
};

function productSelectHandler(){
    $("#pcount").html($("#id_products :selected").length);
}