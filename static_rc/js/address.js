var data = {}
var deliveryPrice = parseFloat(0).toFixed(2);

$(document).ready(function(){
    initialize();
    $("#id_country, #id_city").change(locationChanged);
    $("#id_country").change();
    $("#id_city").change();
});

function initialize(){
    data['country'] = JSON.parse($("#id_country_meta").val());
    data['city'] = JSON.parse($("#id_city_meta").val());
}

function locationChanged(){
    var element = $(this);
    if (element.val()){
        var pk = element.val(), name = element.attr("name");
        var newPrice = parseFloat(data[name][pk]);
        $("#deliveryPrice").html(parseFloat(newPrice).toFixed(2));
    }
}