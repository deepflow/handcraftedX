from django.conf.urls import url
from django.conf.urls import include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from ajax_select import urls as ajax_select_urls
from django.conf import settings
from django.views.generic import TemplateView
from django.contrib.sitemaps.views import sitemap

from common.views import ShortenerView
from .views import LandingView, upload
from .sitemaps import HourlySiteMap, ProductSiteMap, ArticleSiteMap

sitemaps = {
    "index": HourlySiteMap,
    "products": ProductSiteMap,
    "posts": ArticleSiteMap,
}

handler404 = "handcrafted.views.handler404"
handler500 = "handcrafted.views.handler500"

urlpatterns = [
    url("^$", LandingView.as_view(), name="landing"),

    url(r"^store/", include("shop.urls", namespace="shop")),

    url(r"^(?P<code>[\w\W]{1,3})/$", ShortenerView.as_view(), name='shortener'),

    url(r"^common/", include("common.urls", namespace="common")),

    url(r"^selling/", include("selling.urls", namespace="selling")),

    url(r"^baseket/", include("basket.urls", namespace="basket")),

    url(r"^checkout/", include("checkout.urls", namespace="checkout")),

    url(r'^auth/', include("authentication.urls", namespace="auth")),

    url(r'^pages/', include("blog.urls", namespace="blog")),

    url(r'^calipso/', include("calipso.urls", namespace="calipso")),

    # MERGE AJAXABLE TO COMMON
    url(r'^ajaxable/', include("ajaxable.urls", namespace="ajaxable")),

    # UPLOAD VIEW
    url(r'^upload/$', upload, name="upload"),

    url(r"^ajax_select/", include(ajax_select_urls)),

    url(r'^admin/', admin.site.urls),

    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^tinymce/', include('tinymce.urls')),

    # ROBOTS AND SITEMAP
    url(r'^robots\.txt$', TemplateView.as_view(template_name="robots.txt")),

    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +\
static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [url(r'^admin/translate/', include('rosetta.urls')),]
