from django.views import generic
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from shop.models import Product
from selling.models import Seller
from blog.models import Article
from calipso.models import WordCloudItem

from common.models import Slide

class LandingView(generic.TemplateView):
    template_name = "landing.html"

    def get_slider(self):
        return Slide.objects.order_by("?").filter(position="0")

    def get_brands(self, count=3):
        # return Seller.objects.order_by("?")[:count]
        return Seller.objects.all()[:count]

    def get_featured(self, count=3):
        return Product.get_actives().order_by("?")[:count]

    def get_posts(self, count=2):
        return Article.objects.filter(is_visible=True).order_by("?")[:count]
    
    # finish this up
    def get_products(self):
        """
            returns dict of products containing 
            popular, latest_3c, random_3c, random_3c
        """
        queryset = Product.get_actives()
        popular = queryset.order_by("-rank")[:3]
        return "fuck"
    
    def get_topseller(self, count=3):
        #how to get topseller objects?
        return Product.get_actives().order_by("?")[:count]
    
    def get_latest(self, count=3):
        return Product.get_actives().order_by("-created")[:count]

    def get_popular(self, count=10):
        return Product.get_actives().order_by("-rank")[:count]

    def get_random(self, count=3):
        return Product.get_actives().order_by("?")[:count]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['topseller'] = self.get_topseller()
        context['latest'] = self.get_latest()
        context['popular'] = self.get_popular()
        context['random'] = self.get_random()

        context['product_slider_title'] = _("პოპულარული")
        context['slider'] = self.get_slider()
        context['brands'] = self.get_brands()
        context['featured'] = self.get_featured()
        context['posts'] = self.get_posts()
        return context


def handler404(request):
    context = {"error_code": "404", "show_search": True}
    context['message'] = _('''
        უი დაიკარგე?! სამწუხაროდ ვერ ვიპოვეთ რასაც ეძებდი, მაგრამ არაუშავს,
        შენ არ იდარდო! დარწმუნდი, რომ ბმული სწორად შეიყვანე ან უბრალოდ
        ქვემოთ საძიებო ველი გამოიყენე:
    ''')
    return render(request, "error_page.html", context)

def handler500(request):
    context = {"error_code": "500", "show_search": False}
    context['message'] = _('''
        სამწუხაროდ, ჩვენს დეველოპერებს ყავა დააკლდათ და კოდის წერის დროს 
        შეცდომა დაუშვეს. შეტყობინება უკვე გაგზავნილია. შეგიძლიათ იმავეს გაკეთება
        მოგვიანებით სცადოთ, ან თუ ძალიან საჩქაროა, მოგვწერეთ!
    ''')
    return render(request, "error_page.html", context)


from django.http import HttpResponse, JsonResponse
from common.models import Image

def upload(request):
    if request.POST:
        entry = Image.objects.create(file=request.POST['target_file'])
        return JsonResponse({"response": entry.pk})
    return HttpResponse("OK")
