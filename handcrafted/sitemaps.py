from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from shop.models import Product
from blog.models import Article

class HourlySiteMap(Sitemap):
    changefreq = "hourly"
    priority = "0.9"
    
    def __init__(self):
        self.urls = {
            "index": reverse("landing"),
            "blog": reverse("blog:list"),
            "authentication": reverse("auth:signin"),
            "store": reverse("shop:main"),
        }

    def items(self):
        return list(self.urls.keys())
    
    def location(self, item):
        return self.urls[item]


class ProductSiteMap(Sitemap):
    changefreq = "weekly"
    priority = "1.0"

    def items(self):
        return Product.get_actives()

    def location(self, item):
        return item.get_absolute_url()


class ArticleSiteMap(Sitemap):
    changefreq = "weekly"
    priority = "1.0"

    def items(self):
        return Article.objects.filter(is_visible=True)

    def location(self, item):
        return item.get_absolute_url()
