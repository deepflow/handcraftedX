from django.contrib.sessions.models import Session
from basket.models import Basket


class BasketMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not request.user.is_authenticated() and not hasattr(request.user, "basket"):
            request.session.save()
            setattr(request.user, "basket", self.get_basket(request))
        response = self.get_response(request)
        return response

    def get_basket(request):
        session = Session.objects.get(session_key=request.session.session_key)
        return Basket.objects.get_or_create(session=session)[0]