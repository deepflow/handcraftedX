from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r"psearch/(?P<query>[-\w\W]+)/$", SearchView.as_view(), name="psearch"),

    url(r'search/$', SearchView.as_view(), name="search"),

    url(r"bitbot/$", BitBotView.as_view(), name="bitbot"),

    url(r"visionX/$", VisionXView.as_view(), name="visionX"),

    url(r"grid/$", GridView.as_view(), name="grid"),
]
