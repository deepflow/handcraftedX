from django.contrib import admin
from .models import WordCloudItem
x = admin.site.register

class WordCloudItemAdmin(admin.ModelAdmin):
    list_display = ("word", "count")

x(WordCloudItem, WordCloudItemAdmin)
