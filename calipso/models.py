from django.db import models
from django.utils.translation import ugettext_lazy as _

from common.models import SnapShot

class WordCloudItem(models.Model):
    word = models.CharField(_("სიტყვა"), max_length=64, db_index=True)
    count = models.IntegerField(_("რაოდენობა"))

    def __str__(self):
        return self.word

    class Meta:
        verbose_name = _("სიტყვების ღრუბლის ერთეული")
        verbose_name = _("სიტყვების ღრუბლის ერთეულები")
        ordering = ['-count']

class TraceLog(SnapShot):
    '''Trace Log model for tracing user navigation data'''
    #user
    #url
    pass