from sklearn.feature_extraction.text import CountVectorizer

from django.utils.html import strip_tags

from shop.models import Product

def get_contents(exObjPk):
    contents = []
    queryset = Product.get_actives().exclude(pk=exObjPk)
    for product in queryset:
        contents.append(strip_tags(product.desc))
    return contents

def get_word_bag(contents):
    """
        converts content list to word bag
    """
    contents = get_contents(3)
    #minimum document frequency, words occured less than min_df time, drop that word
    vectorizer = CountVectorizer(min_df=1)
    ft = vectorizer.fit_transform(contents)
    fns = vectorizer.get_feature_names()
    print(fns)
    print(ft.toarray().transpose())
