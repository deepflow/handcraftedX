from django.http import JsonResponse
from django.shortcuts import render
from django.views import generic
from django.db.models import Q

from shop.models import Product
from common.models import Keyword, Tokenizer, URLink

class SearchView(generic.TemplateView):
    template_name = "shop/product_list.html"

    def get_objects(self):
        query = self.kwargs.get("query", "mnbvqweret3421wqe")
        ques = Q(desc__icontains=query) | Q(name__icontains=query)
        return Product.get_actives().filter(ques)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = self.get_objects()
        return context

# TEST VIEW
class SearchView(generic.ListView):
    template_name = "calipso/search_results.html"

    def get_query(self):
        """
            returns query as a list of keywordable results
            get_query() -> list
        """
        query = self.request.GET.get("query", "")
        query = [str(x) for x in query.split(" ")] # split words
        query = [x[:-1] for x in query if len(x) > 3] # extract reasonable values
        return query

    def get_keywords(self, query):
        ques = Q()
        for q in query:
            Q(base__icontains=q)
        return None

    def get(self, request, *args, **kwargs):
        query = self.get_query()
        
        return render(request, self.template_name, self.get_context_data(**kwargs))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


# TEST PROJECT
from django.db.models import Q

class BitBotView(generic.TemplateView):
    template_name = "calipso/bitbot.html"

    def get(self, request, *args, **kwargs):
        if request.GET.get("backlink", None):
            # ajax call was initiated
            data = {"response": "ok"}
            # first step is generate keyword associated product short url
            query = [str(x) for x in request.GET.get("query").split(" ")]
            query = [x[:-1] for x in query if len(x) > 4]
            ques = Q()
            for q in query:
                ques |= Q(base__icontains=q)
            keywords = None
            if ques:
                keywords = Keyword.objects.filter(ques)
                if keywords.count():
                    
                    token = Tokenizer.get_new_token()
                    token.meta = data
                    token.save()
            else:
                keywords = Keyword.objects.none()
            print("found - ", keywords.count(), keywords)
            # 2nd step is to match price
            
            # 3rd step is to

            return JsonResponse(data)
        return render(request, self.template_name, self.get_context_data(**kwargs))


from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

@method_decorator(csrf_exempt, name="dispatch")
class VisionXView(generic.TemplateView):
    template_name = ""

    def post(self, request, *args, **kwargs):
        ''' '''
        if self.request.POST.get("image_upload", None):
            # image upload logic is initiated, return url of image
            keywords = VisionX.get_image_tags(request.FILES.get("image", None),
                host=request.get_host())
            return JsonResponse({"keywords": keywords})
        elif self.request.POST.get("keyword_goc", None):
            # create keyword and returns it's pk and __str__
            kpk = VisionX.keyword_goc(self.request.POST.get("foreign_name", None))
            return JsonResponse({"kwd_pk": kpk})
        return JsonResponse({"ok": "ok"})

from common.models import Image, Keyword
import cloudsight, time

class VisionX:
    @staticmethod
    def get_image_tags(image, host):
        '''upload image and return it's url on the server'''
        if image is not None:
            entry = Image.objects.create(image=image)
            api = cloudsight.API(cloudsight.SimpleAuth("6FlakYDOXpN_rZn2csVxlA"))
            resp = api.remote_image_request("http://" + host + entry.get_absolute_url(), {
                "image_request[locale]": "en-US"})
            status = api.wait(resp['token'], timeout=20)
            return status['name'].split(" ")
        return []

    @staticmethod
    def keyword_goc(fname):
        '''get or create keyword with given name'''
        try:
            return Keyword.objects.get(fname=fname).pk
        except Keyword.DoesNotExist:
            return Keyword.objects.create(name="", fname=fname).pk


class GridView(generic.TemplateView):
    template_name = "calipso/grid.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Product.get_actives().order_by("?")[:60]
        return context