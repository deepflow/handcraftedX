import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from django.utils.html import strip_tags

from shop.models import Product
from .models import WordCloudItem

class WordCloud:
    '''
        Class for creating word clouds using countvectorizer
    '''
    stopWords = ["nbsp", "და", "ან", "ის", "ეს", "მე", "ჩემი", "შენ", "შენი",
        "მისი", "ყიდვა", "ყიდვის", "ერთი", "ორი", "სამი", "ოთხი", "მის",
        "ჩვენ", "ჩვენი", "არჩევანი", "სტანდარტული"]

    def __init__(self):
        '''
            init function automatically does the job and saves words
            and it's occurence counts to databse, if reload, database
            entries are recreated based on new values, olds removed
            __init__ -> None
        '''
        contents = self.get_contents()
        bag = self.get_word_bag(contents)
        self.save_words(bag[0], bag[1])

    def get_contents(self):
        '''
            Method for getting contents from Product objects for further
            processing
            get_contents() -> list
        '''
        contents = []
        for p in Product.get_actives():
            contents.append(strip_tags(p.desc))
            contents.append(p.name)
        return contents

    def get_word_bag(self, contents):
        '''
            method for calculating unique words and their count
            in contents list
            get_word_bag(contents) -> [feature_names, occurence_count]
        '''
        vectorizer = CountVectorizer(analyzer="word", stop_words=self.stopWords)
        ft = vectorizer.fit_transform(contents)
        fns = vectorizer.get_feature_names()
        #ft.toarray().shape -> row(sample) x col(feature)
        ft = ft.toarray().sum(axis=0) #0 === x, and sums all rows to one
        return [fns, ft]

    def save_words(self, feature_names, occurence_count):
        '''
            saves/updates feature names and their counts to database
            save_words(feature_names, occurence_count) -> None
        '''
        for x in range(len(feature_names))DE    :
            WordCloudItem.objects.update_or_create(word=feature_names[x],
                count=occurence_count[x])
