from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r"^$", ArticleListView.as_view(), name="list"),
    url(r"^(?P<page_name>[-\w\W]+)/$", ArticleView.as_view(), name="page"),
]