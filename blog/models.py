from django.db import models
from ckeditor.fields import RichTextField
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html, strip_tags

from django.core.urlresolvers import reverse

from common.models import SnapShot
from common.models import URLink

class Article(SnapShot):
    image = models.ImageField(_("სურათი"), upload_to="blog/", blank=True)

    title_ka = models.CharField(_("სტატიის სახელი"), max_length=128, blank=True)
    title_en = models.CharField(_("სტატიის სახელი"), max_length=128, blank=True)
    title_ru = models.CharField(_("სტატიის სახელი"), max_length=128, blank=True)
    content_ka = RichTextField(_("სტატიის კონტენტი"), blank=True)
    content_en = RichTextField(_("სტატიის კონტენტი"), blank=True)
    content_ru = RichTextField(_("სტატიის კონტენტი"), blank=True)
    is_visible = models.BooleanField(_("გამოჩნდეს ვებ გვერდზე?"), default=False)
    page_name = models.CharField(_("გვერდის სახელი"), help_text=_("არ დაგავიწყდეთ სლეშები"),
        unique=True, max_length=128)

    def get_title(self):
        return self.title_ka

    def get_content(self):
        return self.content_ka

    def get_short_content(self):
        return " ".join(strip_tags(self.content_ka).split(" ")[:10])

    def get_absolute_url(self):
        return reverse("blog:page", kwargs={"page_name": self.page_name})

    def get_short_link(self):
        matrix = "<a href=/{0}/ target='_blank'>{0}</a>"
        try:
            short_link = URLink.objects.filter(rev=self.get_absolute_url())[0].code
            return format_html(matrix.format(short_link))
        except Exception:
            return self.get_absolute_url()
    get_short_link.short_description = _("მოკლე ბმული")

    def __str__(self):
        return self.title_ka

    def save(self, *args, **kwargs):
        if self.pk is None:
            URLink.objects.create(rev=self.get_absolute_url())
        super().save(*args, **kwargs)
    
    class Meta:
        verbose_name = _("სტატია")
        verbose_name_plural = _("სტატიები")
    