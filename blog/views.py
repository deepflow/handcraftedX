from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views import generic

from .models import Article

# Create your views here.
class ArticleListView(generic.ListView):
    template_name = 'blog/list.html'
    queryset = Article.objects.filter(is_visible=True)

class ArticleView(generic.TemplateView):
    template_name = "blog/detail.html"

    def get_object(self):
        page_name = self.kwargs.get("page_name", None)
        return get_object_or_404(Article, page_name=page_name)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.get_object()
        return context
