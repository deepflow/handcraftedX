from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class BlogConfig(AppConfig):
    name = 'blog'
    verbose_name = _("ბლოგი")