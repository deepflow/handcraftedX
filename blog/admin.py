from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import *

x = admin.site.register

class ArticleAdmin(admin.ModelAdmin):
    fieldsets = (
        (_("ზოგადი"), {
            "fields": ("page_name", "image", "is_visible",)
        }),
        (_("ქართული ვარიანტი"), {
            "fields": ("title_ka", "content_ka",),
            "classes": ("collapse",)
        })
    )
    list_display = ("page_name", "get_title", "is_visible", "get_short_link")

x(Article, ArticleAdmin)
