from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from .models import URLink, Tokenizer
from shop.models import ProductOption
from selling.models import Seller
from authentication.models import InternalMessage

import ast

class ShortenerView(generic.TemplateView):
    def get(self, request, *args, **kwargs):
        object = None
        try:
            object = URLink.objects.get(code=kwargs.get("code", None))
            return redirect(object.rev)
        except URLink.DoesNotExist:
            return redirect("auth:signup")


class MessageToSellerView(generic.TemplateView):
    template_name = "common/messageTo.html"

    def get_option_nseller(self):
        try:
            po = ProductOption.objects.get(pk=self.request.GET.get("opk"))
            return po, po.get_seller()
        except ProductOption.DoesNotExist:
            pass
        return None, None

    def extract_token(self):
        self.token = Tokenizer.get_token(self.kwargs.get("token", None))
        if not self.token:
            return None
        data = ast.literal_eval(self.token.meta)
        self.option = get_object_or_404(ProductOption, pk=data["opk"])
        self.seller = self.option.get_seller()
        return True        
    
    def get(self, request, *args, **kwargs):
        if not self.extract_token():
            messages.add_message(request, messages.SUCCESS, _(
                "არასწორი ბმული, გთხოვთ სცადოთ თავიდან"))
        # define predef text
        pdt = "{0} ({1}{2})".format(self.option, request.META['HTTP_HOST'], 
            self.option.product.get_absolute_url())
        return render(request, self.template_name, self.get_context_data(
            predef=pdt, receiver=self.seller, price=self.option.get_special_price()))

    def post(self, request, *args, **kwargs):
        if not self.extract_token():
            messages.add_message(request, messages.SUCCESS, _(
                "არასწორი ბმული, გთხოვთ სცადოთ თავიდან"))
            return redirect("landing")
        content = request.POST.get("content", None)
        special_price = request.POST.get("special_price", None)
        content += str(_("<<< სპეციალური ფასი - {0} >>>".format(str(special_price))))
        # special option primary key
        InternalMessage.objects.create(owner=self.seller.account, 
            sender=request.user, content=content, token=self.token)
        self.token.update(action=reverse("shop:accept_special", 
            kwargs={"token":self.token.token}))
        self.token.update(sprice=special_price)
        messages.add_message(request, messages.SUCCESS, _(
                "შეტყობინება წარმატებით გაიგზავნა :)"))
        return redirect("landing")
        