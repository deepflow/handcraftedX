from urllib.parse import parse_qsl
import random
from string import ascii_lowercase as al
from string import ascii_uppercase as uc
from string import digits as dg

def get_set():
    temp = [x for x in al + uc + dg]
    random.shuffle(temp)
    return temp

def gen_code():
    return ''.join([random.choice(get_set()) for x in range(3)])

def generate_token(length=None):
    return ''.join([random.choice(get_set()) for x in range(random.randint(5, 10))])

def generate_random_number(length=None):
    return ''.join([random.choice(dg) for x in range(length)])

# generate token for tokenizer
def generate_token_ft():
    return ''.join([random.choice(get_set()) for x in range(24)])

def get_querystring_value(name, qs):
    queryDict = dict(parse_qsl(qs))
    if name in queryDict:
        return queryDict[name]
    return None


# SLUG GENERATION & PROCSSING FUNCTIONS
def latinify(text):
	gec = 'აბგდევზთიკლმნოპჟრსტუფქღყშცჩძწჭხჯჰ'
	enc = 'abgdevzTiklmnopJrstufqRyScCZwWxjh'
	new = ''
	for char in text:
		if char in gec:
			char = enc[gec.index(char)]
		new += char
	return new

def slugify(text):
    text = text.replace(" ", "")
    return latinify(text)

# FACBOOK API INTEGRATION
# import facebook

# def get_api(config):
#     graph = facebook.GraphAPI(config['at'])
#     resp = graph.get_object("me/accounts")
#     page_access_token = None
#     for page in resp['data']:
#         print(page)
#         if page["id"] == config["pid"]:
#             page_access_token = page["access_token"]
#             print(page_access_token)
#     graph = facebook.GraphAPI(page_access_token)
#     return graph


# def main():
#     config = {"pid": "1306536309441395",
#         "at": "EAACgxPt1j0EBAJVacCNEFBW6ivxDNIUEbOjSCDwCh9Qefk1UUHi71cqC7nyjjWxkyayFHJxgbbZCtgUlsYSKK2SKjpHA76GXsfzTjVPwM9XMp3zgD45vbj08kbStsh834bsL7haCwBdPoTylbsmTN77nfKYepF2pGuqbX8yPiZBIENCsyqc0B1NTZCr9P8ZD",}
#     msg = "https://google.com"
#     api = get_api(config)
#     api.put_wall_post(msg)
