from django import forms
from django.utils.translation import ugettext_lazy as _
from django.core.files.images import get_image_dimensions
from .models import Slide, Keyword


class SlideAdminForm(forms.ModelForm):
    IMG_WIDTH_BOUNDS = (1070, 1170)
    IMG_HEIGHT_BOUNDS = (450, 550)

    def clean_image(self):
        image = self.cleaned_data.get("image")
        if not image:
            raise forms.ValidationError(_('გთხოვთ შეავსოთ სურათის ველი'))
        width, height = get_image_dimensions(image)

        if width < self.IMG_WIDTH_BOUNDS[0] or width > self.IMG_WIDTH_BOUNDS[1]:
            raise forms.ValidationError(_("სურათის სიგანე უნდა იყოს 1070-დან" +\
                "1170 პიქსელამდე"))
        elif height < self.IMG_HEIGHT_BOUNDS[0] or \
            height > self.IMG_HEIGHT_BOUNDS[1]:
            raise forms.ValidationError(_("სურათის სიმაღლე უნდა იყოს 450-დან" +\
                "550 პიქსელამდე"))
        return image

    class Meta:
        model = Slide
        fields = "__all__"


class KeywordAdminForm(forms.ModelForm):
    def clean_name(self):
        name = self.cleaned_data['name']
        print("request hit with name = ", name)
        if Keyword.objects.filter(name=name).count() >= 1:
            raise forms.ValidationError(_("მსგავსი საკვანძო სიტყვა უკვე არსებობს"), 
                code="invalid")
        return name

    class Meta:
        fields = ("__all__")
        model = Keyword
