from django.utils.translation import ugettext_lazy as _
from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = 'common'
    verbose_name = _("საერთო")