from django.template import Library
from PIL import Image

register = Library()

@register.filter(name="four_count")
def get_four_count(query_set):
    '''
        given a queryset it returns how many 4 item is in it
        get_four_count(query_set) -> int
    '''
    return range(int(query_set.count() / 4) + 1)

@register.filter(name="get_four")
def get_four(query_set, index):
    '''
        given queryset and index, function returns 4 step items on index
        get_four(query_set, index) -> QuerySet
    '''
    query_set = query_set[(index*4):(index*4 + 4)]
    return query_set
    