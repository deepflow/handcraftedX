from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r"shortix/$", ShortenerView.as_view(), name="shortenerX"),
    url('m2s/(?P<token>[\w\W]+)/$', MessageToSellerView.as_view(), name="m2s"),
]
