import ast
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.core.urlresolvers import resolve
from django.utils.translation import get_language

from .helpers import gen_code, generate_token_ft

class SnapShot(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Slide(models.Model):
    SPC = (
        ("0", _("მთავარი გვერდი top")),
    )

    position = models.CharField(_("სლაიდის პოზიცია"), max_length=1, choices=SPC, default="0")

    image = models.ImageField(upload_to="slide_images", height_field='img_height', width_field='img_width', null=True)
    img_height = models.PositiveIntegerField(blank=True)
    img_width = models.PositiveIntegerField(blank=True)

    caption_url = models.CharField(_("მცირე ტექსტის ბმული"), max_length=64, blank=True, default="#")
    action_url = models.CharField(_("სამოქმედო ბმული"), max_length=64, blank=True)
    caption_text_ka = models.CharField(_("მცირე ტექსტი (ka)"), max_length=32, blank=True, default="#")
    caption_text_en = models.CharField(_("მცირე ტექსტი (en)"), max_length=32, blank=True, default="#")
    caption_text_ru = models.CharField(_("მცირე ტექსტი (ru)"), max_length=32, blank=True, default="#")
    action_text_ka = models.CharField(_("სამოქმედო ტექსტი (ka)"), max_length=32, blank=True)
    action_text_en = models.CharField(_("სამოქმედო ტექსტი (en)"), max_length=32, blank=True)
    action_text_ru = models.CharField(_("სამოქმედო ტექსტი (ru)"), max_length=32, blank=True)
    main_text_ka = models.CharField(_("მთავარი ტექსტი (ka)"), max_length=32, blank=True)
    main_text_en = models.CharField(_("მთავარი ტექსტი (en)"), max_length=32, blank=True)
    main_text_ru = models.CharField(_("მთავარი ტექსტი (ru)"), max_length=32, blank=True)

    @property
    def caption_text(self):
        fname = "caption_text_" + get_language()
        attr = getattr(self, fname, None)
        return attr if attr else self.caption_text_ka

    @property
    def action_text(self):
        fname = "action_text_" + get_language()
        attr = getattr(self, fname, None)
        return attr if attr else self.action_text_ka
    
    @property
    def main_text(self):
        fname = "main_text_" + get_language()
        attr = getattr(self, fname, None)
        return attr if attr else self.main_text_ka

    def __str__(self):
        return "{0}:{1}".format(self.get_position_display(), self.main_text)

    class Meta:
        verbose_name = _("სლაიდი")
        verbose_name_plural = _('სლაიდერები')

class URLink(SnapShot):
    code = models.CharField(_("კოდი"), max_length=3, unique=True)
    rev = models.CharField(_("რევერს/ნორმ. url"), max_length=128)

    @staticmethod
    def get_shortcut(referer):
        entry, created = URLink.objects.get_or_create(rev=referer)
        if created:
            entry.save()
        return reverse("shortener", kwargs={"code": entry.code})

    def __str__(self):
        return self.rev

    def save(self, *args, **kwargs):
        if self.pk is None:
            code = None
            try:
                while True:
                    code = gen_code()
                    URLink.objects.get(code=code)
            except URLink.DoesNotExist:
                self.code = code
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("შემოკლება")
        verbose_name_plural = _("შემოკლებები")
        ordering = ['-created']


class Tokenizer(models.Model):
    token = models.CharField(max_length=24, default=generate_token_ft, db_index=True)
    meta = models.TextField(blank=True, default="")

    @staticmethod
    def get_new_token(obj=False):
        if not obj:
            return Tokenizer.objects.create().token
        return Tokenizer.objects.create()

    @staticmethod
    def get_token(token):
        try:
            return Tokenizer.objects.get(token=token)
        except Tokenizer.DoesNotExist:
            return None

    @staticmethod
    def check_token(token):
        if Tokenizer.objects.filter(token=token).count():
            return True
        return False

    def update(self, **kwargs):
        # updates token meta dict by kwargs
        initial = ast.literal_eval(self.meta)
        initial.update(**kwargs)
        self.meta = str(initial)
        self.save()

    def get_val(self, vname):
        # get value by name from meta dict
        data = ast.literal_eval(self.meta)
        return data[vname] if vname in data else None

    def expire(self):
        self.delete() # mark token to delete it

    def get_products_link(self):
        return URLink.get_shortcut(reverse("shop:deserialize", kwargs={"token": self.token}))

    def get_token_title(self):
        try:
            return self.get_val("title")
        except Exception:
            return "CONVERSION ERROR"
    get_token_title.short_description = _("თოუქენის სათაური")

    def __str__(self):
        return self.token

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.meta = "{}"
        super().save(*args, **kwargs)


class Image(models.Model):
    image = models.ImageField(verbose_name=_("სურათი"), upload_to="uploads", height_field='img_height', width_field='img_width', null=True, blank=True)
    img_height = models.PositiveIntegerField(blank=True, null=True)
    img_width = models.PositiveIntegerField(blank=True, null=True)

    def get_absolute_url(self):
        return self.image.url

    def __str__(self):
        return "Image N{0}".format(self.pk)

    class Meta:
        verbose_name = _("სურათი")
        verbose_name_plural = _("სურათები")

class Keyword(models.Model):
    name = models.CharField(_("სიტყვა"), max_length=32)
    fname = models.CharField(_("არაქართული ანალოგი"), max_length=32, blank=True, default="")
    base = models.CharField(_("ფუძე"), max_length=512, blank=True, default="")
    # relatives = models.CharField(_("ნათესავები"), max_length=512, blank=True, default="")
    # meta = models.CharField(_("მეტა ინფორმაცია"), max_length=128, blank=True, default="")

    def get_absolute_url(self):
        return reverse("shop:keyword_products", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name if get_language() == "ka" else self.fname

    def save(self, *args, **kwargs):
        # ხმოვნით დაბოლოებული სიტყვა - მ, თანხმოვნით - მა
        if len(self.name) > 3:
            self.base = self.name + self.name[:-1] + self.name[:-2]
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = _("საკვანძო სიტყვა")
        verbose_name_plural = _("საკვანძო სიტყვები")
