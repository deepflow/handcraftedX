from django.core.mail import EmailMessage
from django.utils.translation import ugettext_lazy as _

from authentication.models import InternalMessage, Account

def message_admins(message):
    '''
        function for sending email and internal message to admins
        message_admins(str) -> None
    '''
    admins = Account.objects.filter(is_admin=True)
    for admin in admins:
        InternalMessage.objects.create(owner=admin, content=message)
        EmailMessage(_("მოთხოვნა ახალი გამყიდვლის შესახებ"), message,
            to=[admin.email]).send(fail_silently=True)
