from django.contrib import admin
from .models import URLink, Slide, Keyword, Image, Tokenizer
from .forms import SlideAdminForm, KeywordAdminForm

x = admin.site.register

class KeywordAdmin(admin.ModelAdmin):
    form = KeywordAdminForm

    def get_fieldsets(self, request, obj=None):
        if request.user.is_admin:
            return ((None, {"fields": ("name", "fname", "base"),}),)
        return ((None, {"fields": ("name", "fname")}),)

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_admin or obj is None:
            return ("",)
        return ("name", "fname")


class SlideAdmin(admin.ModelAdmin):
    # form = SlideAdminForm
    pass


class TokenizerAdmin(admin.ModelAdmin):
    list_display = ("token", "get_token_title")

class ImageAdmin(admin.ModelAdmin):
    list_display = ("__str__", "get_absolute_url")

x(URLink)
x(Slide, SlideAdmin)
x(Keyword, KeywordAdmin)
x(Image, ImageAdmin)
x(Tokenizer, TokenizerAdmin)