from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r"proto/(?P<raw>[\w\W]+)/$", proto, name="proto"),
]
