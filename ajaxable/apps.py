from django.apps import AppConfig


class AjaxableConfig(AppConfig):
    name = 'ajaxable'
