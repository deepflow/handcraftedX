import ast
from django.http import JsonResponse

from shop.models import Product
from basket.models import Basket
from common.models import URLink
from selling.helpers import message_to_seller
from authentication.models import InternalMessage

def kwarg_test(arg1, arg2):
    print(arg1, arg2)

MAPPING = {
    "upvote": Product.upvote,

    "addtobasket": Basket.sadd_option,
    "rmfrombasket": Basket.srm_option,
    "updateQ": Basket.supdate_quantity,
    # url shortener view
    'shortcut': URLink.get_shortcut,
    # message to sender
    'm2s': message_to_seller,
    # mark message as read
    'markMsgAsRead': InternalMessage.mark_message_as_read,
    'deleteMsg': InternalMessage.delete_message,
}

def callF(raw):
    try:
        temp = raw.split("-")
        result = MAPPING[temp[0]](*temp[1:])
        if result is None:
            return True
        return result
    except Exception as ge:
        print("Exception in AJAXABLE - ", ge)
        return "error"

def callF_rev2(raw):
    try:
        temp = raw.split("-")
        fname = temp[0]
        print("incoming raw = ", temp[1])
        kwargs = ast.literal_eval(temp[1])
        print(fname, kwargs)
        result = MAPPING[fname](**kwargs)
        if result is None:
            return True
        return result
    except Exception as ge:
        print("Exception in AJAXABLE - ", ge)
        return "error"

def proto(request, raw):
    data = {"result": str(callF_rev2(raw))}
    return JsonResponse(data)
